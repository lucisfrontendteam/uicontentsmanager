var path = require('path');
var autoprefixer = require('autoprefixer');
var webpack = require('webpack');
var CleanWebpackPlugin = require('clean-webpack-plugin');

// This shouldn't be exposed to the user.
var isInNodeModules = 'node_modules' ===
  path.basename(path.resolve(path.join(__dirname, '..', '..')));
var relativePath = isInNodeModules ? '../../..' : '..';
var isInDebugMode = process.argv.some(arg =>
  arg.indexOf('--debug-template') > -1
);
if (isInDebugMode) {
  relativePath = '../template';
}
var srcPath = path.resolve(__dirname, relativePath, 'src');
var uicontentsPath= path.resolve(__dirname, '../../resourcecreator', 'src');
var nodeModulesPath = path.join(__dirname, '..', 'node_modules');
var buildPath = path.join(__dirname, isInNodeModules ? '../../..' : '..', 'build');

module.exports = {
  devtool: 'eval-source-map',
  entry: [
    require.resolve('webpack-dev-server/client') + '?http://0.0.0.0:3002',
    require.resolve('webpack/hot/dev-server'),
    path.join(srcPath, 'index'),
  ],
  output: {
    // Next line is not used in dev but WebpackDevServer crashes without it:
    path: buildPath,
    pathinfo: true,
    filename: 'manager.js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['', '.js'],
  },
  resolveLoader: {
    root: nodeModulesPath,
    moduleTemplates: ['*-loader']
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'eslint',
        include: [srcPath, uicontentsPath],
      }
    ],
    loaders: [
      {
        test: /\.js$/,
        include: [srcPath, uicontentsPath],
        loader: 'babel',
        exclude: [path.resolve(srcPath, 'Utils/UIContentsLoader.js')],
        query: require('./babel.dev')
      },
      {
        test: /\.css$/,
        include: [srcPath, uicontentsPath],
        loader: 'style!css!postcss'
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      // {
      //   test: /\.(ttf|woff|woff2|otf)$/,
      //   loader: 'file',
      // },
      // {
      //   test: /\.(jpg|png|gif|eot|svg)$/,
      //   loader: 'base64',
      // },
      {
        test: /\.(ttf|woff|woff2|otf|jpg|png|gif|eot|svg)$/,
        loader: 'file',
      },
      {
        test: /\.(mp4|webm)$/,
        loader: 'url?limit=10000'
      },
      {
        test: require.resolve("jquery"),
        loader: "expose?$!expose?jQuery"
      },
    ]
  },
  eslint: {
    configFile: path.join(__dirname, 'eslint.js'),
    useEslintrc: false,
    parser: 'babel-eslint',
  },
  postcss: function() {
    return [autoprefixer];
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        "NODE_ENV": JSON.stringify(process.env.NODE_ENV),
        "DIST_TYPE": JSON.stringify(process.env.DIST_TYPE),
      }
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
    }),
    // Note: only CSS is currently hot reloaded
    new webpack.HotModuleReplacementPlugin({
      multiStep: true,
    }),
    new CleanWebpackPlugin([buildPath], {
      // Without `root` CleanWebpackPlugin won't point to our
      // project and will fail to work.
      root: process.cwd()
    }),
  ]
};
