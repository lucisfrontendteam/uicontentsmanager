var environment = {
  development: {
    rootPath: '/manager',
  },
  production: {
    rootPath: '/manager',
  },
  dist: {
    rootPath: '/manager',
  },
};

module.exports = environment;
