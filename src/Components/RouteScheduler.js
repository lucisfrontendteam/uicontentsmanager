import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Router, Route, browserHistory, IndexRedirect } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import _ from 'lodash';
import { store, actions } from '../Redux';
import { FailScenario } from './FailScenario';
import {
  MainPage,
  LoginPage,
  ProjectEditorPage,
  ScenarioEditorPage,
  WidgetAllocatorPage,
  ScenarioPermissionPage,
  WidgetManifestEditorPage,
  CategoryEditorPage,
  CategoryInfoPage,
  CounselorEditorPage,
  UserEditorPage,
} from '../Containers/Pages';
import PathManager from '../Utils/PathManager';
import { parse } from 'query-string';

class RouteScheduler extends PureComponent {
  get pathManager() {
    const { props } = this;
    return new PathManager(props);
  }

  handleOnRouteError() {
    const { pathManager } = this;
    browserHistory.push(pathManager.errorPath);
  }

  shouldComponentUpdate(nextProps) {
    const {
      LoginManagerState: nextUserState,
      ProjectState: nextProjectState,
      ProjectManagerState: nextProjectManagerState,
      NetWorkModeState: nextNetWorkModeState,
      CategorySearchKeywordState: nextCategorySearchKeywordState,
      UserFilterState: nextUserFilterState,
    } = nextProps;
    const {
      LoginManagerState: currentUserState,
      ProjectState: currentProjectState,
      ProjectManagerState: currentProjectManagerState,
      NetWorkModeState: currentNetWorkModeState,
      CategorySearchKeywordState: currentCategorySearchKeywordState,
      UserFilterState: currentUserFilterState,
    } = this.props;

    const userStateChanged
      = !!!_.isEqual(nextUserState, currentUserState);
    const projectStateChanged
      = !!!_.isEqual(nextProjectState, currentProjectState);
    const projectListStateChanged
      = !!!_.isEqual(nextProjectManagerState, currentProjectManagerState);
    const networkStateChanged
      = !!!_.isEqual(nextNetWorkModeState, currentNetWorkModeState);
    const categorySearchKeywordStateChanged
      = !!!_.isEqual(nextCategorySearchKeywordState, currentCategorySearchKeywordState);
    const userFilterStateChanged
      = !!!_.isEqual(nextUserFilterState, currentUserFilterState);

    return userStateChanged || projectStateChanged ||
      projectListStateChanged || networkStateChanged ||
      categorySearchKeywordStateChanged || userFilterStateChanged;
  }

  get queryStringObject() {
    const params = parse(location.search);
    return params || { userId: '' };
  }

  get userIdFromQuery() {
    const { userId } = this.queryStringObject;
    return _.isUndefined(userId) ? '' : userId;
  }

  get queryString() {
    const queryString = [];
    if (!!!_.isEmpty(this.userIdFromQuery)) {
      queryString.push('?userId=');
      queryString.push(this.userIdFromQuery);
    }
    return queryString.join('');
  }

  get loginPath() {
    const { pathManager } = this;
    const url = `${pathManager.loginPath}${this.queryString}`;
    return url;
  }

  manageHistory() {
    const { props, pathManager } = this;
    const { LoginManagerState, ProjectState } = props;
    const loggedIn = LoginManagerState.status === true;
    const projectNotFound = _.isEmpty(ProjectState);
    if (loggedIn) {
      if (projectNotFound) {
        browserHistory.push(pathManager.projectSelectPath);
      }
    } else {
      browserHistory.push(this.loginPath);
    }
  }

  componentDidUpdate() {
    this.manageHistory();
  }

  componentDidMount() {
    this.manageHistory();
  }

  render() {
    const history = syncHistoryWithStore(browserHistory, store);
    const { pathManager } = this;
    return (
      <Router history={history} onError={this.handleOnRouteError} >
        <Route path="/" >
          <Route path={pathManager.rootPath} >
            <IndexRedirect to={pathManager.loginPath} />
            <Route path={pathManager.loginPath} component={LoginPage} />
            <Route path={pathManager.homeRootPath} component={MainPage}>
              <Route
                path={pathManager.projectSelectPath}
                components={{ contents: ProjectEditorPage }}
              />
            </Route>
            <Route path={pathManager.userScenarioRootPath} component={MainPage}>
              <Route
                path={pathManager.userScenarioEditPath}
                components={{ contents: ScenarioEditorPage }}
              />
              <Route
                path={pathManager.userScenarioAllocatePath}
                components={{ contents: WidgetAllocatorPage }}
              />
              <Route
                path={pathManager.userScenarioPermissionPath}
                components={{ contents: ScenarioPermissionPage }}
              />
            </Route>
            <Route path={pathManager.widgetRootPath} component={MainPage}>
              <Route
                path={pathManager.widgetManifestPath}
                components={{ contents: WidgetManifestEditorPage }}
              />
            </Route>
            <Route path={pathManager.configRootPath} component={MainPage}>
              <Route
                path={pathManager.configCategoryEditPath}
                components={{ contents: CategoryEditorPage }}
              />
              <Route
                path={pathManager.configCategoryInfoPath}
                components={{ contents: CategoryInfoPage }}
              />
              <Route
                path={pathManager.configCounselorPath}
                components={{ contents: CounselorEditorPage }}
              />
              <Route
                path={pathManager.configPermissionPath}
                components={{ contents: UserEditorPage }}
              />
            </Route>
            <Route path={pathManager.errorPath} component={FailScenario} />
          </Route>
        </Route>
        <Route status="404" path="*" component={FailScenario} />
      </Router>
    );
  }
}

const mapStateToProps = (state) => state;
const connectedRouteScheduler
  = connect(mapStateToProps, actions)(RouteScheduler);
export { connectedRouteScheduler as RouteScheduler };
