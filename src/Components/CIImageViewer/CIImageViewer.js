import React, { PureComponent } from 'react';
import UIContentsLoader from '../../Utils/UIContentsLoader';
const { ProjectWriter } = UIContentsLoader;
import nameTag from '../../Resources/images/logo_content_manager.png';
import './custom.css';

export class CIImageViewer extends PureComponent {
  render() {
    const { props } = this;
    const ciImage = ProjectWriter.getCiImage(props);
    return (
      <div className="logo-area">
        <div className="logo-wrap">
          <img className="logo ci" src={ciImage} alt="VERINT LOGO" />
        </div>
        <div className="logo-wrap2">
          <img className="logo" src={nameTag} alt="Content Manager" />
        </div>
      </div>
    );
  }
}
