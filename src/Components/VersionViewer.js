import React, { PureComponent } from 'react';
import { version as managerVersion } from '../../package.json';
import UIContents from '../Utils/UIContentsLoader';
const { version: uicontentsVersion } = UIContents;

export default class VersionViewer extends PureComponent {
  render() {
    const uicontentsInfo = `UIContents.js: ver ${uicontentsVersion}`;
    const managerInfo = `Manager.js: ver ${managerVersion}`;
    return (
      <ul style={{ float: 'left' }}>
        <li className="dropdown filter-menu">
          <span>{uicontentsInfo}</span>
        </li>
        <li className="dropdown filter-menu">
          <span>{managerInfo}</span>
        </li>
      </ul>
    );
  }
}
