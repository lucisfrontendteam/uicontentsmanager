import React, { PureComponent } from 'react';
import { MenuTree } from './MenuTree';
import UIContnets from '../Utils/UIContentsLoader';
const { TreeTraveser } = UIContnets;

export class MenuTreePlayer extends PureComponent {
  render() {
    const { props } = this;
    const treeTraveser = new TreeTraveser(props.MenuTreeState);
    const { accessPath } = treeTraveser.getRootModel();
    const depth1Models = treeTraveser.getNodeListByPredict(node => node.getPath().length === 2);
    return (
      <div className="gnb">
        <MenuTree
          {...props}
          accessPath={accessPath}
          depth1Models={depth1Models}
        />
      </div>
    );
  }
}
