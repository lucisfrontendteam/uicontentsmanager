import React, { PureComponent } from 'react';
import $ from 'jquery';
import { browserHistory } from 'react-router';
import UIContents from '../Utils/UIContentsLoader';
const { EnvironmentManager } = UIContents;
import environment from '../../config/environment';

export class LoginStatusViewer extends PureComponent {
  handleLogout() {
    const { props } = this;
    const { rootPath } = new EnvironmentManager(environment);
    browserHistory.push(rootPath);
    props.loginmanagerlogoutInitAction(props);
  }

  handleShowLoginStatusViewer() {
    $('#loginStatusViewer').show();
  }

  componentDidMount() {
    $('body').on('click', e => {
      const { id } = e.target;
      if (id !== 'loginStatusViewer') {
        $('#loginStatusViewer').hide();
      }
    });
  }

  componentWillUnmount() {
    $('body').off('click');
  }

  render() {
    const { LoginManagerState } = this.props;
    return (
      <li className="dropdown basic-menu">
        <a
          className="dropdown-toggle"
          data-toggle="dropdown"
          role="button"
          onClick={() => this.handleShowLoginStatusViewer()}
        >{LoginManagerState.userName}</a>
        <ul className="dropdown-menu" id="loginStatusViewer">
          <li><a onClick={() => this.handleLogout()}>Logout</a></li>
        </ul>
      </li>
    );
  }
}
