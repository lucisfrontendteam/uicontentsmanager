import * as actionTypes from './actionTypes';

export function NetWorkModeState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.NETWORK_MODE_OFF: {
      finalState = {
        label: '오프라인 모드',
        value: false,
      };
    } break;

    case actionTypes.NETWORK_MODE_ON: {
      finalState = {
        label: '온라인 모드',
        value: true,
      };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
