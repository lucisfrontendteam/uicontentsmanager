import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import { v4 } from 'uuid';
import _ from 'lodash';
import UIContents from '../Utils/UIContentsLoader';
const { TreeTraveser } = UIContents;
import PathManager from '../Utils/PathManager';

export class MenuTree extends PureComponent {
  static defaultProps = {
    showStyle: {
      display: 'block',
    },
    hideStyle: {
      display: 'none',
    },
  };

  constructor(props) {
    super(props);
    const pathManager = new PathManager(props);
    this.rootPath = pathManager.rootPath;
  }

  get selectedMenuBranch() {
    const { MenuTreeState, location } = this.props;
    const [,,, lastUri] = location.pathname.split('/');
    const treeTraveser = new TreeTraveser(MenuTreeState);
    const targetModel = treeTraveser.getNodeByPredict(node => node.model.uri === lastUri);
    if (_.isUndefined(targetModel)) {
      throw new Error('메뉴를 찾을 수 없습니다.');
    }
    return targetModel.getPath();
  }

  renderMenus(parentModel) {
    const { props, rootPath, selectedMenuBranch } = this;
    return _.map(parentModel.children, nodeModel => {
      const { uri, name } = nodeModel.model;
      const selectedScenarioRoot = selectedMenuBranch[1];
      const selectedScenario = selectedMenuBranch[2];
      const scenarioPath =
        `${rootPath}/${selectedScenarioRoot.model.uri}/${uri}`;
      const scenarioSelected = selectedScenario.model.uri === uri;
      const activeClassName = scenarioSelected ? 'active' : '';
      const scenarioRootSelected = selectedScenarioRoot.model.uri === parentModel.model.uri;
      const visibility = scenarioRootSelected ? props.showStyle : props.hideStyle;

      return (
        <li key={v4()} style={visibility} className={activeClassName}>
          <Link to={scenarioPath} activeClassName={activeClassName}>{name}</Link>
        </li>
      );
    });
  }

  renderMenuRoots() {
    const { props, rootPath, selectedMenuBranch } = this;
    return _.map(props.depth1Models, nodeModel => {
      const { uri, name } = nodeModel.model;
      const selectedScenarioRoot = selectedMenuBranch[1];
      const selected = selectedScenarioRoot.model.uri === uri;
      const visibility = selected ? props.showStyle : props.hideStyle;
      const activeClassName = selected ? 'active' : '';
      const firstScenario = nodeModel.children[0];
      const firstScenarioPath =
        `${rootPath}/${uri}/${firstScenario.model.uri}`;

      return (
        <li className={activeClassName} key={v4()}>
          <Link to={firstScenarioPath} >{name}</Link>
          <ul className="dep2" style={visibility}>
            {this.renderMenus(nodeModel)}
          </ul>
        </li>
      );
    });
  }

  render() {
    return (
      <ul className="dep1">
        {this.renderMenuRoots()}
      </ul>
    );
  }
}
