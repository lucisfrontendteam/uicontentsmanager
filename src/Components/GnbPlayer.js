import React, { PureComponent } from 'react';
import { LoginStatusViewer } from './LoginStatusViewer';
import VersionViewer from './VersionViewer';
import { CIImageViewer } from './CIImageViewer';
import { MenuTreePlayer } from './MenuTreePlayer';
import _ from 'lodash';
import PathManager from '../Utils/PathManager';
import { browserHistory } from 'react-router';
import $ from 'jquery';
import '../Resources/js/notify';

export class GnbPlayer extends PureComponent {
  get pathManager() {
    const { props } = this;
    return new PathManager(props);
  }

  checkSelectedProject() {
    const { props, pathManager } = this;
    const {
      ProjectState,
      location,
      LoginManagerState = {
        status: false,
      },
    } = props;
    const loggedOut =
      LoginManagerState.status === false ||
      _.isUndefined(LoginManagerState.status);
    if (loggedOut) {
      browserHistory.push(pathManager.loginPath);
    } else {
      const projectNotFound = _.isEmpty(ProjectState);
      const isNotProjectSelectPage =
        pathManager.projectSelectPath !== location.pathname;
      if (projectNotFound && isNotProjectSelectPage) {
        $.notify('편집할 프로젝트를 선택하여 주십시오', 'error');
        browserHistory.push(pathManager.projectSelectPath);
      }
    }
  }

  componentDidUpdate() {
    this.checkSelectedProject();
  }

  componentDidMount() {
    this.checkSelectedProject();
  }

  render() {
    const { props } = this;
    return (
      <header className="main-header">
        <div className="main-header-inner">
          <CIImageViewer {...props} />
          <nav className="gnb-area">
            <div className="top-menu">
              <VersionViewer {...props} />
              <ul>
                <LoginStatusViewer {...props} />
              </ul>
            </div>
            <MenuTreePlayer {...props} />
          </nav>
        </div>
      </header>
    );
  }
}
