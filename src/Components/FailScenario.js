import React, { PureComponent } from 'react';
export class FailScenario extends PureComponent {
  static defaultProps = {
    strong1: '잘못된 경로',
    strong2: '가 입력되었습니다.',
    message: '요청하신 페이지를 찾을 수 없습니다. 주소를 다시 한번 확인하여 주시길 바랍니다.',
  };

  render() {
    const { strong1, strong2, message } = this.props;
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="row">
            <div className="col-md-12">
              <div className="error-wrap">
                <div className="error-in">
                  <table className="error-con">
                    <tbody>
                      <tr>
                        <td>
                          <p className="error-msg">
                            <strong>{strong1}</strong>{strong2}
                          </p>
                          <p className="error-msg-s">
                            {message}
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
