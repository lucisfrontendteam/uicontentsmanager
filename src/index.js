import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './Redux';
import { RouteScheduler } from './Components/RouteScheduler';

window.onload = () => {
  const rootElement = document.getElementById('root');
  ReactDOM.render(
    <Provider store={store}>
      <RouteScheduler />
    </Provider>,
    rootElement
  );
};
