import React, { PureComponent } from 'react';
import UIContents from '../Utils/UIContentsLoader';
import { connect } from 'react-redux';
import { actions } from '../Redux';
import { GnbPlayer } from '../Components/GnbPlayer';

class Scenario extends PureComponent {
  render() {
    const { props } = this;
    return (
      <div>
        <GnbPlayer {...props} />
        <UIContents.WidgetViewer {...props} />
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
const connectedScenario = connect(mapStateToProps, actions)(Scenario);
export { connectedScenario as Scenario };
