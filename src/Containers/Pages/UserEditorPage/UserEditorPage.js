import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../../Redux';
import $ from 'jquery';
import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/droppable';
import _ from 'lodash';
import { v4 } from 'uuid';
import 'bootstrap';
import './custom.css';
import '../../../Resources/js/notify';
import { UserForm } from './UserForm';
import PathManager from '../../../Utils/PathManager';
import UIContents from '../../../Utils/UIContentsLoader';
const { TextRenderer, PropsCreator } = UIContents;

class UserEditorPage extends PureComponent {
  get pathManager() {
    const { props } = this;
    return new PathManager(props);
  }

  componentDidMount() {
    const { props } = this;
    const newProps = PropsCreator.getUserManageProps(props);
    props.usermanagerreadInitAction(newProps);
  }

  shouldComponentUpdate(nextProps) {
    const {
      UserManagerState: nextUserManagerState,
      UserFilterState: nextUserFilterState,
    } = nextProps;
    const {
      UserManagerState: currentUserManagerState,
      UserFilterState: currentUserFilterState,
    } = this.props;

    const userFilterStateChanged
      = !!!_.isEqual(nextUserFilterState, currentUserFilterState);
    const userMangerStateChanged
      = !!!_.isEqual(nextUserManagerState, currentUserManagerState);

    return userFilterStateChanged || userMangerStateChanged;
  }

  handleModify(e, record) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    props.userSelectAction(record.userId);
  }

  handleNewUser() {
    $('#userFormLayer').modal('show');
  }

  handleRemove(e, targetRecord) {
    e.preventDefault();
    e.stopPropagation();
    const question = `${targetRecord.userName}을(를) 삭제하시겠습니까?`;

    // eslint-disable-next-line
    if (confirm(question)) {
      const { props } = this;
      const newProps = PropsCreator.getUserManageProps(props, targetRecord);
      props.usermanagerdeleteInitAction(newProps);
    }
  }

  handleSearch(e) {
    e.preventDefault();
    e.stopPropagation();
    const keyword = $('#userFilter').val();
    const { props } = this;
    props.userFilterInitAction(keyword);
  }

  renderUserRecords() {
    const { props } = this;
    const { UserManagerState } = props;
    const records = _.map(UserManagerState, record => {
      const {
        userName,
        userId,
        userEmail,
        userOrganization,
        regDate,
      } = record;
      const formattedRegDate
        = TextRenderer.renderDateTimeString(regDate, 'YYYYMMDD', 'YYYY.MM.DD');
      return (
        <tr key={v4()} className="line">
          <td>{userName}</td>
          <td>{userId}</td>
          <td>{userEmail}</td>
          <td>{userOrganization}</td>
          <td>{formattedRegDate}</td>
          <td>
            <div
              className="btn-group"
              role="group"
              aria-label="유저 레코드 컨트롤"
            >
              <button
                type="button"
                className="btn btn-xs btn-secondary btn-warning"
                data-toggle="modal"
                onClick={e => this.handleModify(e, record)}
              >수정</button>
              <button
                type="button"
                className="btn btn-xs btn-secondary btn-danger"
                data-toggle="modal"
                onClick={e => this.handleRemove(e, record)}
              >삭제</button>
            </div>
          </td>
        </tr>
      );
    });
    return (
      <tbody>
        {records}
      </tbody>
    );
  }

  render() {
    const { props } = this;
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="row">
            <div className="col-md-12">
              <div className="grid-wrap">
                <div className="grid-header">
                  <div className="row">
                    <div className="col-md-3">
                      <h2 className="grid-input">
                        <div className="pos-r">
                          <form role="search" onSubmit={e => this.handleSearch(e)}>
                            <label className="admin-placeholder">검색</label>
                            <input
                              id="userFilter"
                              type="text"
                              placeholder="검색"
                              className="form-control"
                            />
                          </form>
                        </div>
                      </h2>
                      <div className="pos-right">
                        <button type="button" className="btn-tool btn-search"></button>
                      </div>
                    </div>
                  </div>
                  <div className="pos-right">
                    <button
                      type="button"
                      className="btn-tool btn-plus"
                      data-toggle="modal"
                      onClick={() => this.handleNewUser()}
                    >사용자 추가</button>
                  </div>
                </div>
                <div className="grid-body">
                  <table className="list-table">
                    <thead>
                      <tr>
                        <th>사용자명</th>
                        <th>아이디</th>
                        <th>이메일</th>
                        <th>소속</th>
                        <th>등록일</th>
                        <th>편집</th>
                      </tr>
                    </thead>
                    {this.renderUserRecords()}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
        <UserForm {...props} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { UserFilterState, UserManagerState } = state;

  let newState = {};
  if (!!!_.isEmpty(UserFilterState)) {
    const newUserManagerState
      = _.filter(UserManagerState, user => {
        const {
          userName,
          userId,
          userEmail,
          regDate,
          userOrganization,
        } = user;
        const userNameFound = userName.indexOf(UserFilterState) !== -1;
        const userIdFound = userId.indexOf(UserFilterState) !== -1;
        const userEmailFound = userEmail.indexOf(UserFilterState) !== -1;
        const regDateFound = regDate.indexOf(UserFilterState) !== -1;
        const userOrganizationFound = userOrganization.indexOf(UserFilterState) !== -1;

        return userNameFound || userIdFound ||
          userEmailFound || regDateFound ||
          userOrganizationFound;
      });
    newState = {
      ...state,
      UserManagerState: newUserManagerState,
    };
  } else {
    newState = { ...state };
  }
  return newState;
};
const connectedPermissionEditorPage
  = connect(mapStateToProps, actions)(UserEditorPage);
export { connectedPermissionEditorPage as UserEditorPage };
