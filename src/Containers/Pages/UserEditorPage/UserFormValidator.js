import _ from 'lodash';
import UIContents from '../../../Utils/UIContentsLoader';
const { CommonManagerManifest } = UIContents;

export default class UserFormValidator {
  static textBoxEmpty(value) {
    return _.isEmpty(value);
  }

  static lengthOver(value = '', maxLength) {
    return value.length > maxLength;
  }

  static dupFind(list, callback) {
    const duplication = _.find(list, callback);
    return !!!_.isEmpty(duplication);
  }

  static invalidEmailAddress(value) {
    // eslint-disable-next-line
    const pattern = new RegExp(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i);
    const valid = pattern.test(value);
    return !!!valid;
  }

  static nothingSelected(selectedNodes) {
    return _.isEmpty(selectedNodes);
  }

  static getManifestRecordByKey(key) {
    const { UserManagerNewManifest } = CommonManagerManifest;
    const manifest =
      _.find(UserManagerNewManifest.inputIdList, el => el.key === key);
    return manifest;
  }

  static validateUserId(list, userId, isEditMode) {
    const empty = UserFormValidator.textBoxEmpty(userId);
    const {
      desc,
      length: userIdLength,
    } = UserFormValidator.getManifestRecordByKey('userId');
    const lengthOver = UserFormValidator.lengthOver(userId, userIdLength);
    const dupFind
      = isEditMode ? false : UserFormValidator.dupFind(list, record => record.userId === userId);

    let message = '';
    if (empty) {
      message += `${desc}은(는) 필수 입력입니다.`;
    }
    if (lengthOver) {
      message += `${desc}은(는) ${userIdLength}자를 넘을 수 없습니다.`;
    }
    if (dupFind) {
      message += `${desc}은(는) 중복될 수 없습니다.`;
    }

    return {
      errorFound: empty || lengthOver || dupFind,
      message,
    };
  }

  static validateUserName(userName) {
    const empty = UserFormValidator.textBoxEmpty(userName);
    const {
      desc,
      length: userNameLength,
    } = UserFormValidator.getManifestRecordByKey('userName');
    const lengthOver = UserFormValidator.lengthOver(userName, userNameLength);

    let message = '';
    if (empty) {
      message += `${desc}은(는) 필수 입력입니다.`;
    }
    if (lengthOver) {
      message += `${desc}은(는) ${userNameLength}자를 넘을 수 없습니다.`;
    }

    return {
      errorFound: empty || lengthOver,
      message,
    };
  }

  static validateUserEmail(userEmail) {
    // const empty = UserFormValidator.textBoxEmpty(userEmail);
    const {
      desc,
      length: userEmailLength,
    } = UserFormValidator.getManifestRecordByKey('userEmail');
    const lengthOver = UserFormValidator.lengthOver(userEmail, userEmailLength);
    // const invalidEmail = UserFormValidator.invalidEmailAddress(userEmail);

    let message = '';
    // if (empty) {
    //   message += `${desc}은(는) 필수 입력입니다.`;
    // }
    if (lengthOver) {
      message += `${desc}은(는) ${userEmailLength}자를 넘을 수 없습니다.`;
    }
    // if (invalidEmail) {
    //   message += '올바르지 않은 이메일 주소가 입력되었습니다.';
    // }

    return {
      errorFound: lengthOver,
      message,
    };
  }

  static validateUserPass(userPass) {
    const empty = UserFormValidator.textBoxEmpty(userPass);
    const {
      desc,
      length: userPassLength,
    } = UserFormValidator.getManifestRecordByKey('userName');
    const lengthOver = UserFormValidator.lengthOver(userPass, userPassLength);

    let message = '';
    if (empty) {
      message += `${desc}은(는) 필수 입력입니다.`;
    }
    if (lengthOver) {
      message += `${desc}은(는) ${userPassLength}자를 넘을 수 없습니다.`;
    }

    return {
      errorFound: empty || lengthOver,
      message,
    };
  }

  static validateUserOrganization(userOrganization) {
    const empty = UserFormValidator.textBoxEmpty(userOrganization);
    const {
      desc,
      length: userOrganizationLength,
    } = UserFormValidator.getManifestRecordByKey('userOrganization');
    const lengthOver = UserFormValidator.lengthOver(userOrganization, userOrganizationLength);

    let message = '';
    if (empty) {
      message += `${desc}은(는) 필수 입력입니다.`;
    }
    if (lengthOver) {
      message += `${desc}은(는) ${userOrganizationLength}자를 넘을 수 없습니다.`;
    }

    return {
      errorFound: empty || lengthOver,
      message,
    };
  }

  static validateUserScenarioTree(selectedNodes) {
    const empty = UserFormValidator.nothingSelected(selectedNodes);
    const {
      desc,
    } = UserFormValidator.getManifestRecordByKey('userScenarioTree');

    let message = '';
    if (empty) {
      message += `${desc}은(는) 필수 입력입니다.`;
    }

    return {
      errorFound: empty,
      message,
    };
  }

  static validateOrganizationTree(selectedNodes) {
    const empty = UserFormValidator.nothingSelected(selectedNodes);
    const {
      desc,
    } = UserFormValidator.getManifestRecordByKey('userOrganizationTree');

    let message = '';
    if (empty) {
      message += `${desc}은(는) 필수 입력입니다.`;
    }

    return {
      errorFound: empty,
      message,
    };
  }
}
