import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../../Redux';
import _ from 'lodash';
import $ from 'jquery';
import { v4 } from 'uuid';
import '../../../Resources/js/notify';
import UserFormValidator from './UserFormValidator';
import UIContents from '../../../Utils/UIContentsLoader';
const { PropsCreator, TreeTraveser } = UIContents;

class UserForm extends PureComponent {
  static defaultProps = {
    title: '사용자 추가',
  };

  get userId() {
    return $('#userId').val();
  }

  get userName() {
    return $('#userName').val();
  }

  get userEmail() {
    return $('#userEmail').val();
  }

  get userPass() {
    return $('#userPass').val();
  }

  get userOrganization() {
    return $('#userOrganization').val();
  }

  get selectedUserScenarioTree() {
    return this.$userScenarioTree.tree('getSelectedNode');
  }

  get selectedOrganizationTree() {
    const { selected_node: selectedNode } = this.$userOrganizationTree.tree('getState');
    const groupNodes = _.map(selectedNode, nodeId => {
      const targetNode = this.$userOrganizationTree.tree('getNodeById', nodeId);
      return targetNode;
    });
    return groupNodes;
  }

  getSelectedNodeList($targetjqTree) {
    const {
      selected_node: selectedNodeIdList,
    } = $targetjqTree.tree('getState');

    const selectedNodes = _.map(_.compact(selectedNodeIdList), nodeId => {
      const node = $targetjqTree.tree('getNodeById', nodeId);
      const { id, name } = node;
      return { id, name };
    });
    return selectedNodes;
  }

  get userScenarioTree() {
    const selectedNodes = this.getSelectedNodeList(this.$userScenarioTree);
    return selectedNodes;
  }

  get userOrganizationTree() {
    const selectedNodes = this.getSelectedNodeList(this.$userOrganizationTree);
    return selectedNodes;
  }

  validateUserId() {
    const { props } = this;
    const { UserManagerState } = props;
    const {
      errorFound,
      message,
    } = UserFormValidator.validateUserId(UserManagerState, this.userId, this.isEditMode);
    if (errorFound) {
      $.notify(message, 'error');
    }
    return !!!errorFound;
  }

  validateUserName() {
    const {
      errorFound,
      message,
    } = UserFormValidator.validateUserName(this.userName);
    if (errorFound) {
      $.notify(message, 'error');
    }
    return !!!errorFound;
  }

  validateUserEmail() {
    const {
      errorFound,
      message,
    } = UserFormValidator.validateUserEmail(this.userEmail);
    if (errorFound) {
      $.notify(message, 'error');
    }
    return !!!errorFound;
  }

  validateUserPass() {
    const {
      errorFound,
      message,
    } = UserFormValidator.validateUserPass(this.userPass);
    if (errorFound) {
      $.notify(message, 'error');
    }
    return !!!errorFound;
  }

  validateUserOrganization() {
    const {
      errorFound,
      message,
    } = UserFormValidator.validateUserOrganization(this.userOrganization);
    if (errorFound) {
      $.notify(message, 'error');
    }
    return !!!errorFound;
  }

  validateUserScenarioTree() {
    const {
      errorFound,
      message,
    } = UserFormValidator.validateUserScenarioTree(this.selectedUserScenarioTree);
    if (errorFound) {
      $.notify(message, 'error');
    }
    return !!!errorFound;
  }

  validateUserOrganizationTree() {
    const {
      errorFound,
      message,
    } = UserFormValidator.validateOrganizationTree(this.selectedOrganizationTree);
    if (errorFound) {
      $.notify(message, 'error');
    }
    return !!!errorFound;
  }

  get userData() {
    return {
      id: v4(),
      userName: this.userName,
      userId: this.userId,
      userPass: this.userPass,
      userEmail: this.userEmail,
      userOrganization: this.userOrganization,
      userScenarioTree: this.userScenarioTree,
      userOrganizationTree: this.userOrganizationTree,
    };
  }

  validateForm() {
    const userIdValid = this.validateUserId();
    const userNameValid = this.validateUserName();
    const userEmailValid = this.validateUserEmail();
    const userPassValid = this.validateUserPass();
    const userOrganizationValid = this.validateUserOrganization();
    const userScenarioTreeValid = this.validateUserScenarioTree();
    const userOrganizationTreeValid = this.validateUserOrganizationTree();
    return userIdValid && userNameValid &&
      userEmailValid && userPassValid &&
      userOrganizationValid && userScenarioTreeValid &&
      userOrganizationTreeValid;
  }

  handleSubmit(e) {
    e.preventDefault();
    e.stopPropagation();
    const valid = this.validateForm();
    if (valid) {
      const { props } = this;
      const newProps = PropsCreator.getUserManageProps(props, this.userData);
      if (this.isEditMode) {
        props.usermanagerupdateInitAction(newProps);
      } else {
        props.usermanagernewInitAction(newProps);
      }
      $('#userFormLayer').modal('hide');
    }
  }

  resetInputBoxes() {
    $('#userForm')[0].reset();
  }

  clearTree($targetjqTree) {
    const {
      selected_node: selectedNodeIdList,
    } = $targetjqTree.tree('getState');

    _.each(selectedNodeIdList, nodeId => {
      const selectedNode = $targetjqTree.tree('getNodeById', nodeId);
      $targetjqTree.tree('removeFromSelection', selectedNode);
      _.each(selectedNode.children, child => {
        $targetjqTree.tree('removeFromSelection', child);
      });
    });
  }

  resetUserScenarioTree() {
    this.clearTree(this.$userScenarioTree);
    this.clearTree(this.$userOrganizationTree);
  }

  handleReset(e) {
    e.preventDefault();
    e.stopPropagation();
    this.resetInputBoxes();
    this.resetUserScenarioTree();
    const { props, EditUserState } = this;
    if (!!!_.isEmpty(EditUserState)) {
      props.userSelectAction(EditUserState.userId);
    }
  }

  get userOrganizationSelectState() {
    const { props, EditUserState = this.defaultEditUserState } = this;
    const { userOrganizationTree } = EditUserState;
    const { UserOrganizationTreeState } = props;
    const state = this.getTreeSelectState(UserOrganizationTreeState, userOrganizationTree);
    return state;
  }

  get userScenarioSelectState() {
    const { props, EditUserState = this.defaultEditUserState } = this;
    const { userScenarioTree } = EditUserState;
    const { UserScenarioTreeState } = props;
    const state = this.getTreeSelectState(UserScenarioTreeState, userScenarioTree);
    return state;
  }

  getTreeSelectState(targetTreeState, targetEditState) {
    const treeTraverser = new TreeTraveser(targetTreeState);
    const groupNodes = treeTraverser.getGroupNodes();
    const openNodes = _.map(groupNodes, node => node.model.id);
    const selectedNode = _.map(targetEditState, node => node.id);
    return {
      open_nodes: openNodes,
      selected_node: selectedNode,
    };
  }

  setTreeSelectState($targetTree, state) {
    $targetTree.tree('setState', state);
  }

  renderUserScenarioTree() {
    const { props } = this;
    this.$userScenarioTree = $('#tree1').tree({
      data: props.UserScenarioTreeState,
      autoOpen: true,
      buttonLeft: false,
      selectable: true,
    }).bind('tree.click', e => this.handleTreeNodeClick(e, this.$userScenarioTree));
    this.setTreeSelectState(this.$userScenarioTree, this.userScenarioSelectState);
  }

  renderOrganizationTree() {
    const { props } = this;
    this.$userOrganizationTree = $('#tree2').tree({
      data: props.UserOrganizationTreeState,
      autoOpen: true,
      buttonLeft: false,
      selectable: true,
      onCreateLi(node, $li) {
        if (node.children.length === 0) {
          $li.html('');
        }
      },
    })
    .bind('tree.click', e => this.handleOrgTreeNodeClick(e, this.$userOrganizationTree));
    this.setTreeSelectState(this.$userOrganizationTree, this.userOrganizationSelectState);
  }

  destroyUserScenarioTree() {
    this.$userScenarioTree.tree('destroy');
  }

  destroyOrganizationTree() {
    this.$userOrganizationTree.tree('destroy');
  }

  handleOrgTreeNodeClick(e, $targetTree) {
    e.preventDefault();
    const selectedNode = e.node;
    const selected = $targetTree.tree('isNodeSelected', selectedNode);
    if (selected) {
      $targetTree.tree('removeFromSelection', selectedNode);
    } else {
      $targetTree.tree('addToSelection', selectedNode);
    }
  }

  handleTreeNodeClick(e, $targetTree) {
    e.preventDefault();
    const selectedNode = e.node;
    const selected = $targetTree.tree('isNodeSelected', selectedNode);
    const groupNode = !!!_.isEmpty(selectedNode.children);
    if (selected) {
      if (groupNode) {
        $targetTree.tree('removeFromSelection', selectedNode);
        _.each(selectedNode.children, child => {
          const childNotFound = _.isEmpty(child.children);
          if (childNotFound) {
            $targetTree.tree('removeFromSelection', child);
          }
        });
      } else {
        const { children } = selectedNode.parent;
        const selectedChildren =
          _.filter(children, child => $(child.element).attr('aria-selected') === 'true');
        if (selectedChildren.length === 1) {
          $targetTree.tree('removeFromSelection', selectedNode.parent);
        }
        $targetTree.tree('removeFromSelection', selectedNode);
      }
    } else {
      if (groupNode) {
        $targetTree.tree('addToSelection', selectedNode);
        _.each(selectedNode.children, child => {
          const childNotFound = _.isEmpty(child.children);
          if (childNotFound) {
            $targetTree.tree('addToSelection', child);
          }
        });
      } else {
        const { element: parentElement } = selectedNode.parent;
        const parentNotSelected = $(parentElement).attr('aria-selected') !== 'true';
        if (parentNotSelected) {
          $targetTree.tree('addToSelection', selectedNode.parent);
        }
        $targetTree.tree('addToSelection', selectedNode);
      }
    }
  }

  handleListNodeClick(e, $targetTree) {
    e.preventDefault();
    const selectedNode = e.node;
    const selected = $targetTree.tree('isNodeSelected', selectedNode);
    const groupNode = !!!_.isEmpty(selectedNode.children);
    if (selected) {
      if (groupNode) {
        $targetTree.tree('removeFromSelection', selectedNode);
        _.each(selectedNode.children, child => {
          const childNotFound = _.isEmpty(child.children);
          if (childNotFound) {
            $targetTree.tree('removeFromSelection', child);
          }
        });
      } else {
        const selectedChildren
          = _.filter(this.getSelectedNodeList($targetTree), node => node.id !== 'dummy');
        if (selectedChildren.length === 1) {
          $targetTree.tree('removeFromSelection', selectedNode.parent);
        }
        $targetTree.tree('removeFromSelection', selectedNode);
      }
    } else {
      if (groupNode) {
        $targetTree.tree('addToSelection', selectedNode);
        _.each(selectedNode.children, child => {
          const childNotFound = _.isEmpty(child.children);
          if (childNotFound) {
            $targetTree.tree('addToSelection', child);
          }
        });
      } else {
        const { children } = selectedNode.parent;
        const selectedChildren = this.getSelectedNodeList($targetTree);
        if (selectedChildren.length + 1 === children.length) {
          $targetTree.tree('addToSelection', selectedNode.parent);
        }
        $targetTree.tree('addToSelection', selectedNode);
      }
    }
  }

  componentDidUpdate() {
    this.destroyOrganizationTree();
    this.destroyUserScenarioTree();
    this.renderUserScenarioTree();
    this.renderOrganizationTree();
    if (this.isEditMode) {
      $('#userFormLayer').modal('show');
    }
  }

  componentDidMount() {
    this.renderUserScenarioTree();
    this.renderOrganizationTree();

    $('#userFormLayer')
      .on('shown.bs.modal', (e) => this.resetInputBoxes(e))
      .on('hidden.bs.modal', (e) => this.handleReset(e));
  }

  componentWillUnmount() {
    this.destroyOrganizationTree();
    this.destroyUserScenarioTree();
  }

  get EditUserState() {
    const { props } = this;
    const { EditUserState } = props;
    return EditUserState;
  }

  get isEditMode() {
    return !!!_.isEmpty(this.EditUserState);
  }

  get disableProps() {
    return this.isEditMode ? { disabled: 'disabled' } : {};
  }

  get title() {
    return this.isEditMode ? '사용자 수정' : '사용자 추가';
  }

  get defaultEditUserState() {
    return {
      userName: '',
      userId: '',
      userPass: '',
      userEmail: '',
      userOrganization: '',
      userScenarioTree: [],
      userOrganizationTree: [],
    };
  }

  render() {
    const {
      userName: defaultUserName,
      userId: defaultUserId,
      userPass: defaultUserPass,
      userEmail: defaultUserEmail,
      userOrganization: defaultUserOrganization,
    } = this.EditUserState || this.defaultEditUserState;
    return (
      <div className="modal fade" tabIndex="-1" role="dialog" id="userFormLayer">
        <div className="modal-dialog" role="document" style={{ width: '1200px' }}>
          <form id="userForm" role="search" onSubmit={(e) => this.handleSubmit(e)}>
            <section className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h1 className="modal-title">{this.title}</h1>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-4">
                    <div className="grid-wrap">
                      <div className="grid-header">
                        <h2 className="grid-title">사용자 정보 입력</h2>
                      </div>
                      <div className="grid-body">
                        <table className="layout-table">
                          <colgroup>
                            <col style={{ width: '25%' }} />
                          </colgroup>
                          <tbody>
                            <tr>
                              <th>아이디</th>
                              <td>
                                <div className="pos-r">
                                  <label className="admin-placeholder">아이디</label>
                                  <input
                                    id="userId"
                                    type="text"
                                    placeholder="아이디"
                                    className="form-control"
                                    maxLength="18"
                                    autoComplete="false"
                                    defaultValue={defaultUserId}
                                    {...this.disableProps}
                                  />
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th>성명</th>
                              <td>
                                <input
                                  id="userName"
                                  type="text"
                                  className="form-control"
                                  placeholder="성명"
                                  maxLength="18"
                                  autoComplete="false"
                                  defaultValue={defaultUserName}
                                  {...this.disableProps}
                                />
                              </td>
                            </tr>
                            <tr>
                              <th>이메일</th>
                              <td>
                                <div className="pos-r">
                                  <label className="admin-placeholder">이메일</label>
                                  <input
                                    id="userEmail"
                                    type="email"
                                    placeholder="이메일"
                                    className="form-control"
                                    maxLength="256"
                                    autoComplete="false"
                                    defaultValue={defaultUserEmail}
                                  />
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th>패스워드</th>
                              <td>
                                <div className="pos-r">
                                  <label className="admin-placeholder">패스워드</label>
                                  <input
                                    id="userPass"
                                    type="password"
                                    placeholder="패스워드"
                                    className="form-control"
                                    maxLength="18"
                                    autoComplete="false"
                                    defaultValue={defaultUserPass}
                                  />
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th>소속</th>
                              <td>
                                <div className="pos-r">
                                  <label className="admin-placeholder">소속</label>
                                  <input
                                    id="userOrganization"
                                    type="text"
                                    placeholder="소속"
                                    className="form-control"
                                    maxLength="18"
                                    autoComplete="false"
                                    defaultValue={defaultUserOrganization}
                                  />
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="grid-wrap">
                      <div className="grid-header">
                        <h2 className="grid-title">사용자가 볼 수 있는 시나리오 선택</h2>
                      </div>
                      <div className="grid-body ico-checkbox" id="tree1" />
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="grid-wrap">
                      <div className="grid-header">
                        <h2 className="grid-title">사용자가 볼 수 있는 조직 데이터 선택</h2>
                      </div>
                      <div className="grid-body ico-checkbox" id="tree2" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="submit"
                  className="btn btn-default btn-modal"
                >확인</button>
                <button
                  type="button"
                  className="btn btn-default btn-modal"
                  data-dismiss="modal"
                  onClick={e => this.handleReset(e)}
                >취소</button>
              </div>
            </section>
          </form>
        </div>
      </div>
    );
  }
}

const getEditUserState = state => {
  const { UserManagerState } = state;
  const editUser = _.find(UserManagerState, user => user.selected);
  return editUser;
};

const getUserScenarioTreeState = state => {
  const { ProjectState } = state;
  let UserScenarioTreeState = [];
  if (ProjectState && ProjectState.children) {
    UserScenarioTreeState = [...ProjectState.children];
  }
  return UserScenarioTreeState;
};

const getUserOrganizationTreeState = state => {
  const { AdvencedFilterArgManagerState } = state;
  const { organizationTree } = AdvencedFilterArgManagerState;
  return organizationTree;
};

const mapStateToProps = (state) => {
  const newState = {
    ...state,
    UserScenarioTreeState: getUserScenarioTreeState(state),
    UserOrganizationTreeState: getUserOrganizationTreeState(state),
    EditUserState: getEditUserState(state),
  };
  return newState;
};
const connectedUserForm
  = connect(mapStateToProps, actions)(UserForm);
export { connectedUserForm as UserForm };
