import React, { PureComponent } from 'react';
import $ from 'jquery';
import 'jqtree';

export class ScenarioPermissionPage extends PureComponent {
  initializeTrees() {
    const self = this;
    const { props } = self;
    const { organizationTree } = props.AdvencedFilterArgManagerState;
    this.$orgTree = $('#tree1').tree({
      data: organizationTree || [],
      autoOpen: true,
      buttonLeft: false,
    });
  }

  destroyTrees() {
    const { $orgTree } = this;
    if ($orgTree) {
      $orgTree.tree('destroy');
    }
  }

  componentDidUpdate() {
    this.destroyTrees();
    this.initializeTrees();
  }

  render() {
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="row">
            <div className="col-md-6 col-pd-r">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">사용자 조직도</h2>
                </div>
                <div className="grid-body ico-checkbox" id="tree1" />
              </div>
            </div>
            <div className="col-md-6 col-pd-l">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">화면 권한</h2>
                  <div className="pos-right">
                    <button
                      type="button"
                      className="btn-tool btn-plus"
                      data-toggle="modal"
                    >권한 추가</button>
                    <button
                      type="button"
                      className="btn-tool btn-minus"
                    >삭제</button>
                  </div>
                </div>
                <div className="grid-body ico-none" id="tree2" />
              </div>
            </div>
            <div className="select-btn-area">
              <button
                type="button"
                className="btn-img-select left"
              >추가</button>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
