import React, { PureComponent } from 'react';

export class CounselorEditorPage extends PureComponent {
  render() {
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="btn-tab-area">
            <ul>
              <li>
                <a
                  href="UI-10-11-01.html"
                  className="btn btn-tab-group active"
                >Group</a>
              </li>
              <li>
                <a
                  href="UI-10-11-02.html"
                  className="btn btn-tab-user"
                >User</a>
              </li>
            </ul>
          </div>
          <div className="row">
            <div className="col-md-3">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">조직</h2>
                </div>
                <div className="grid-body" id="tree1" />
              </div>
            </div>
            <div className="col-md-9">
              <div className="grid-wrap">
                <div className="grid-header">
                  <div className="pos-right">
                    <button
                      type="button"
                      className="btn-tool btn-plus"
                    >조직추가</button>
                    <button
                      type="button"
                      className="btn-tool btn-minus"
                    >조직삭제</button>
                    <button
                      type="button"
                      className="btn-tool btn-reset"
                    >삭제취소</button>
                  </div>
                </div>
                <div className="grid-body">
                  <table className="list-table">
                    <colgroup>
                      <col style={{ width: '20%' }} span="5" />
                    </colgroup>
                    <thead>
                      <tr>
                        <th>상담원 성명</th>
                        <th>사번</th>
                        <th>CTI-ID</th>
                        <th>입사일</th>
                        <th>삭제 여부</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Around the Horn</td>
                        <td>6473</td>
                        <td>3233</td>
                        <td>2016.10.11</td>
                        <td>N</td>
                      </tr>
                      <tr>
                        <td>Around the Horn</td>
                        <td>6473</td>
                        <td>3233</td>
                        <td>2016.10.11</td>
                        <td>N</td>
                      </tr>
                      <tr>
                        <td>Around the Horn</td>
                        <td>6473</td>
                        <td>3233</td>
                        <td>2016.10.11</td>
                        <td>N</td>
                      </tr>
                      <tr>
                        <td>Around the Horn</td>
                        <td>6473</td>
                        <td>3233</td>
                        <td>2016.10.11</td>
                        <td>N</td>
                      </tr>
                      <tr>
                        <td>Around the Horn</td>
                        <td>6473</td>
                        <td>3233</td>
                        <td>2016.10.11</td>
                        <td>N</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
