import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../../Redux';
import { v4 } from 'uuid';
import _ from 'lodash';
import $ from 'jquery';
import 'bootstrap';
import ImageCtrl from './ImageCtrl';
// import UIContents from '../../../Utils/UIContentsLoader';
// const { PropsCreator } = UIContents;

class ProjectForm extends PureComponent {
  get title() {
    return '프로젝트 수정';
  }

  handleSubmit(e) {
    e.preventDefault();
    e.stopPropagation();
    // eslint-disable-next-line
    alert('준비중입니다.');
  }

  resetForm(e) {
    e.preventDefault();
    e.stopPropagation();
    $('#projectForm')[0].reset();
  }

  modalReset() {
    $('#projectFormLayer')
      .on('shown.bs.modal', e => this.resetForm(e))
      .on('hidden.bs.modal', e => this.handleReset(e));
  }

  handleReset(e) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    const { TargetProject } = props;
    props.projectEditEndAction(TargetProject.id);
  }

  showLayer() {
    const { props } = this;
    if (props.TargetProject.isEditMode === true) {
      $('#projectFormLayer').modal('show');
    } else {
      $('#projectFormLayer').modal('hide');
    }
  }

  componentDidMount() {
    this.modalReset();
  }

  shouldComponentUpdate(nextProps) {
    const {
      TargetProject: nextTargetProject,
    } = nextProps;
    const {
      TargetProject: currentTargetProject,
    } = this.props;
    return !!!_.isEqual(nextTargetProject, currentTargetProject);
  }

  componentDidUpdate() {
    this.showLayer();
  }

  render() {
    const { props } = this;
    const {
      // id: projectId,
      thumbnail = '',
      ci = '',
      siteName = '',
      createdAt = '',
      owner = '',
    } = props.TargetProject;

    return (
      <div className="modal fade" tabIndex="-1" role="dialog" id="projectFormLayer">
        <div className="modal-dialog" role="document" style={{ width: '750px' }}>
          <form id="projectForm" role="search" onSubmit={(e) => this.handleSubmit(e)}>
            <section className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h1 className="modal-title">{this.title}</h1>
              </div>
              <div className="modal-body">
                <div className="project-list">
                  <div className="thumbnail-area">
                    <ImageCtrl
                      id={v4()}
                      type="thumb"
                      base64Image={thumbnail}
                      width={158}
                      height={188}
                    />
                  </div>
                  <div className="info-area">
                    <dl className="ci-img">
                      <dt>CI 이미지:<p className="pixel">(135x30픽셀)</p></dt>
                      <dd>
                        <ImageCtrl
                          id={v4()}
                          type="ci"
                          base64Image={ci}
                          width={135}
                          height={30}
                        />
                      </dd>
                    </dl>
                    <dl>
                      <dt>사이트명:</dt>
                      <dd>
                        <input
                          type="text"
                          className="form-control"
                          defaultValue={siteName}
                        />
                      </dd>
                    </dl>
                    <dl>
                      <dt>생성일시:</dt>
                      <dd>
                        <input
                          type="text"
                          className="form-control"
                          defaultValue={createdAt}
                        />
                      </dd>
                    </dl>
                    <dl>
                      <dt>Owner:</dt>
                      <dd>
                        <input
                          type="text"
                          className="form-control"
                          defaultValue={owner}
                        />
                      </dd>
                    </dl>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="submit"
                  className="btn btn-default btn-modal"
                >확인</button>
                <button
                  type="button"
                  className="btn btn-default btn-modal"
                  data-dismiss="modal"
                  onClick={e => this.handleReset(e)}
                >취소</button>
              </div>
            </section>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const TargetProject = _.find(state.ProjectManagerState, project => project.isEditMode) || {};
  return { ...state, TargetProject };
};
const connectedProjectForm = connect(mapStateToProps, actions)(ProjectForm);
export { connectedProjectForm as ProjectForm };
