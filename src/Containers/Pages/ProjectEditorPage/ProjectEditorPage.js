import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import _ from 'lodash';
import { SplitButton, MenuItem, Button } from 'react-bootstrap';
import './custom.css';
import UIContents from '../../../Utils/UIContentsLoader';
const {
  CommonManagerAdapters,
  PropsCreator,
  WidgetDataComparator,
} = UIContents;
import { ProjectForm } from './ProjectForm';

export class ProjectEditorPage extends PureComponent {
  updateProjectList() {
    const { props } = this;
    props.projectmanagerreadInitAction(props, true);
  }

  componentDidMount() {
    this.updateProjectList();
  }

  handleProjectSelect(e, projectId) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    props.projectInitAction(projectId, props);
    const newProps = PropsCreator.getUserManageProps(props);
    props.usermanagerreadInitAction(newProps);
  }

  handleProjectDelete(e) {
    e.preventDefault();
    e.stopPropagation();
    // eslint-disable-next-line
    alert('준비중입니다.');
  }

  handleProjectEditStart(e) {
    e.preventDefault();
    e.stopPropagation();
    // eslint-disable-next-line
    alert('준비중입니다.');
  }

  // handleProjectDelete(e, projectId, siteName) {
  //   e.preventDefault();
  //   e.stopPropagation();
  //   const msg = `프로젝트 ${siteName}을(를) 삭제하시겠습니까?`;
  //   // eslint-disable-next-line
  //   const deleteToBe = window.confirm(msg);
  //   if (deleteToBe) {
  //     const { props } = this;
  //     const newProps = PropsCreator.getProjectEditProps(props, projectId);
  //     props.projectmanagerremoveInitAction(newProps);
  //   }
  // }

  // handleProjectEditStart(e) {
  //   e.preventDefault();
  //   e.stopPropagation();
  //   const { props } = this;
  //   const { id: projectId } = props.ProjectState;
  //   props.projectEditStartAction(projectId);
  // }

  // handleProjectNew(e) {
  //   e.preventDefault();
  //   e.stopPropagation();
  //   $('#projectFormLayer').modal('show');
  // }

  handleProjectNew(e) {
    e.preventDefault();
    e.stopPropagation();
    // eslint-disable-next-line
    alert('준비중입니다.');
  }

  isSelectedProject(projectId) {
    const { ProjectState } = this.props;
    return ProjectState.id === projectId;
  }

  getSelectedProjectClassName(projectId) {
    const selected = this.isSelectedProject(projectId);
    const className = selected ? 'col-md-6 selected' : 'col-md-6';
    return className;
  }

  getProjectNormalControl(projectId) {
    return (
      <Button onClick={(e) => this.handleProjectSelect(e, projectId)}>프로젝트 선택</Button>
    );
  }

  getSelectedProjectControl(projectId, siteName) {
    return (
      <SplitButton
        bsStyle="info"
        title="선택됨"
        style={{ color: '#383030' }}
        id={v4()}
      >
        <MenuItem
          eventKey="1"
          onClick={(e) => this.handleProjectEditStart(e)}
        >프로젝트 수정</MenuItem>
        <MenuItem divider />
        <MenuItem
          eventKey="2"
          bsClass="info"
          onClick={(e) => this.handleProjectDelete(e, projectId, siteName)}
        >프로젝트 삭제</MenuItem>
      </SplitButton>
    );
  }

  renderProjectControlPannel(projectId, selected, siteName) {
    let component = {};
    if (selected) {
      component = this.getSelectedProjectControl(projectId, siteName);
    } else {
      component = this.getProjectNormalControl(projectId);
    }
    return component;
  }

  renderPleaseAddNewProject() {
    return (
      <div className="pleaseAddNew">
        <span onClick={(e) => this.handleProjectNew(e)}>새로운 프로젝트를 추가하십시오</span>
      </div>
    );
  }

  renderProjectList() {
    const { props } = this;
    const { ProjectManagerState } = props;
    const projectListChunks = _.chunk(ProjectManagerState, 2);
    const projectList = _.map(projectListChunks, projectListChunk => {
      const projectChunk = _.map(projectListChunk, project => {
        const {
          thumbnail,
          id: projectId,
          siteName,
          owner,
          createdAt,
          ci,
        } = project;
        const projectClassName = this.getSelectedProjectClassName(projectId);
        const selected = this.isSelectedProject(projectId);
        const thumbnailId = v4();
        const ciId = v4();
        return (
          <div name="project" className={projectClassName} key={v4()}>
            <div className="project-list">
              <div htmlFor={thumbnailId} className="thumbnail-area">
                <label className="input-file-label">
                  <p className="pixel">(155x188픽셀)</p>
                  <div className="img-area">
                    <img src={thumbnail} alt="" className="uploadDisable" />
                  </div>
                </label>
                <input
                  type="file"
                  id={thumbnailId}
                  name="thumbNailUploader"
                  multiple="false"
                  disabled="disabled"
                />
              </div>
              <div className="info-area">
                <dl className="ci-img">
                  <dt>CI 이미지:<p className="pixel">(높이 30px)</p></dt>
                  <dd>
                    <label htmlFor={ciId} className="input-file-label">
                      <div className="img-area">
                        <img src={ci} alt="" className="uploadDisable ci" />
                      </div>
                    </label>
                    <input
                      type="file"
                      id={ciId}
                      name="ciUploader"
                      multiple="false"
                      disabled="disabled"
                    />
                  </dd>
                </dl>
                <dl>
                  <dt>사이트명:</dt>
                  <dd>
                    <input
                      type="text"
                      className="form-control"
                      defaultValue={siteName}
                      readOnly="readonly"
                    />
                  </dd>
                </dl>
                <dl>
                  <dt>생성일시:</dt>
                  <dd>
                    <input
                      type="text"
                      className="form-control"
                      defaultValue={createdAt}
                      readOnly="readonly"
                    />
                  </dd>
                </dl>
                <dl>
                  <dt>Owner:</dt>
                  <dd>
                    <input
                      type="text"
                      className="form-control"
                      defaultValue={owner}
                      readOnly="readonly"
                    />
                  </dd>
                </dl>
                <dl>
                  <dd>
                    {this.renderProjectControlPannel(projectId, selected, siteName)}
                  </dd>
                </dl>
              </div>
            </div>
          </div>
        );
      });
      return (
        <div className="row" key={v4()}>
          {projectChunk}
        </div>
      );
    });
    return projectList;
  }

  renderErrorRecords(errors) {
    const messages = _.map(errors, error => (<p key={v4()}>{error}</p>));
    return messages;
  }

  renderCoreComponent() {
    const { props } = this;
    const { ProjectManagerAdapter } = CommonManagerAdapters;
    const { ProjectManagerState } = props;
    let coreComponent = {};
    if (_.isEmpty(ProjectManagerState)) {
      coreComponent = this.renderPleaseAddNewProject();
    } else {
      const adapter = new ProjectManagerAdapter(ProjectManagerState);
      if (WidgetDataComparator.valid(adapter)) {
        coreComponent = this.renderProjectList();
      } else {
        coreComponent = (
          <div className="row" key={v4()}>
            {this.renderErrorRecords(adapter.errors)}
          </div>
        );
      }
    }
    return coreComponent;
  }

  render() {
    const { props } = this;
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="row">
            <div className="col-md-12">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">프로젝트 리스트</h2>
                  <div className="pos-right">
                    <button
                      type="button"
                      className="btn-tool btn-plus"
                      onClick={(e) => this.handleProjectNew(e)}
                    >새로운 프로젝트 추가</button>
                  </div>
                </div>
                <div className="grid-body project-list-wrap">
                  {this.renderCoreComponent()}
                </div>
              </div>
            </div>
          </div>
        </section>
        <ProjectForm {...props} />
      </div>
    );
  }
}
