import React, { PureComponent } from 'react';

export default class ImageCtrl extends PureComponent {
  componentDidMount() {
    this.updateCanvas();
  }

  componentDidUpdate() {
    this.updateImage();
    this.updateCanvas();
  }

  updateCanvas() {
    const { props, canvasId, imgId } = this;
    const { base64Image } = props;
    if (base64Image) {
      const canvas = document.getElementById(canvasId);
      const ctx = canvas.getContext('2d');
      const image = new Image();
      image.onload = () => {
        ctx.drawImage(image, 0, 0, image.width, image.height,
                          0, 0, canvas.width, canvas.height);
      };
      image.src = base64Image;
      document.getElementById(imgId).src = base64Image;
    }
  }

  get imgId() {
    const { props } = this;
    return `${props.id}-img`;
  }

  get fileCtrlId() {
    const { props } = this;
    return `${props.id}-file`;
  }

  get canvasId() {
    const { props } = this;
    return `${props.id}-canvas`;
  }

  handleImageChange(e) {
    e.preventDefault();
    e.stopPropagation();
    const canvas = document.getElementById(this.canvasId);
    const ctx = canvas.getContext('2d');
    const fr = new FileReader();
    fr.onload = ev => {
      const image = new Image();
      image.onload = () => {
        ctx.drawImage(image, 0, 0, image.width, image.height,
                          0, 0, canvas.width, canvas.height);
      };
      const { result } = ev.target;
      image.src = result;
      document.getElementById(this.imgId).src = result;
    };
    fr.readAsDataURL(e.target.files[0]);
  }

  updateImage() {
    const { props } = this;
    const img = document.getElementById(this.imgId);
    img.src = props.base64Image;
  }

  renderCi() {
    const { props, fileCtrlId, canvasId, imgId } = this;
    const { width, height, base64Image } = props;
    return (
      <div>
        <label htmlFor={fileCtrlId} className="input-file-label">
          <div className="img-area">
            <img id={imgId} src={base64Image} alt="" className="ci" />
          </div>
        </label>
        <input
          type="file"
          id={fileCtrlId}
          multiple="false"
          onChange={e => this.handleImageChange(e)}
        />
        <canvas id={canvasId} width={width} height={height} style={{ display: 'none' }} />
      </div>
    );
  }

  renderThumbNail() {
    const { props, fileCtrlId, canvasId, imgId } = this;
    const { width, height, base64Image } = props;
    return (
      <div>
        <label htmlFor={fileCtrlId} className="input-file-label">
          <p className="pixel">(155x188픽셀)</p>
          <div className="img-area">
            <img id={imgId} src={base64Image} alt="" className="ci" />
          </div>
        </label>
        <input
          type="file"
          id={fileCtrlId}
          multiple="false"
          onChange={e => this.handleImageChange(e)}
        />
        <canvas id={canvasId} width={width} height={height} style={{ display: 'none' }} />
      </div>
    );
  }

  render() {
    const { props } = this;
    let component = '';
    switch (props.type) {
      case 'ci': {
        component = this.renderCi();
      } break;

      case 'thumb':
      default: {
        component = this.renderThumbNail();
      } break;
    }
    return component;
  }
}
