// import _ from 'lodash';
// import UIContents from '../../../Utils/UIContentsLoader';
// const { CommonManagerManifest } = UIContents;
//
// export default class ProjectFormValidator {
//   static empty(value) {
//     return _.isEmpty(value);
//   }
//
//   static lengthOver(value = '', maxLength) {
//     return value.length > maxLength;
//   }
//
//   static dupFind(list, callback) {
//     const duplication = _.find(list, callback);
//     return !!!_.isEmpty(duplication);
//   }
//
//   static getManifestRecordByKey(key) {
//     const { UserManagerNewManifest } = CommonManagerManifest;
//     const manifest =
//       _.find(UserManagerNewManifest.inputIdList, el => el.key === key);
//     return manifest;
//   }
//
//   static validateThumbnail(list, userId, isEditMode) {
//     const empty = UserFormValidator.textBoxEmpty(userId);
//     const {
//       desc,
//       length: userIdLength,
//     } = UserFormValidator.getManifestRecordByKey('userId');
//     const lengthOver = UserFormValidator.lengthOver(userId, userIdLength);
//     const dupFind
//       = isEditMode ? false : UserFormValidator.dupFind(list, record => record.userId === userId);
//
//     let message = '';
//     if (empty) {
//       message += `${desc}은(는) 필수 입력입니다.`;
//     }
//     if (lengthOver) {
//       message += `${desc}은(는) ${userIdLength}자를 넘을 수 없습니다.`;
//     }
//     if (dupFind) {
//       message += `${desc}은(는) 중복될 수 없습니다.`;
//     }
//
//     return {
//       errorFound: empty || lengthOver || dupFind,
//       message,
//     };
//   }
// }
