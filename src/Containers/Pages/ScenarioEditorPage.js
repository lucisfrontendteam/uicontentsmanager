import React, { PureComponent } from 'react';
import projectThemeAimage from '../../Resources/images/@project_theme_a.jpg';
import projectThemeBimage from '../../Resources/images/@project_theme_b.jpg';

export class ScenarioEditorPage extends PureComponent {
  render() {
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="row">
            <div className="col-md-4">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">시나리오</h2>
                  <div className="pos-right">
                    <button type="button" className="btn-tool btn-plus">추가</button>
                    <button type="button" className="btn-tool btn-minus">삭제</button>
                  </div>
                </div>
                <div className="grid-body" id="tree1">
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">프로젝트 테마</h2>
                </div>
                <div className="grid-body">
                  <div className="project-theme-wrap">
                    <h3>Verint Basic</h3>
                    <a className="active"><img src={projectThemeAimage} alt="" /></a>
                  </div>
                  <div className="project-theme-wrap">
                    <h3>Verint Ext#1</h3>
                    <a><img src={projectThemeBimage} alt="" /></a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="grid-wrap accordion">
                <div className="collapse-group" id="accordion02">
                  <div className="panel">
                    <div className="grid-header">
                      <h2 className="grid-title">페이지 기본속성</h2>
                      <div className="pos-right">
                        <a
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion02"
                          href="#collapse02-1"
                          className="btn-collapse"
                        />
                      </div>
                    </div>
                    <div className="panel-collapse collapse in" id="collapse02-1">
                      <div className="page-basic-info">
                        <dl>
                          <dt>제목:</dt>
                          <dd>
                            <div className="pos-r">
                              <label
                                className="admin-placeholder"
                              >FCR(First-Call Resolution)</label>
                              <input
                                type="text"
                                placeholder="FCR(First-Call Resolution)"
                                className="form-control"
                              />
                            </div>
                          </dd>
                        </dl>
                        <dl>
                          <dt>영문명:</dt>
                          <dd>
                            <div className="pos-r">
                              <label
                                className="admin-placeholder"
                              >URL에 사용할 영문 id</label>
                              <input
                                type="text"
                                placeholder="URL에 사용할 영문 id"
                                className="form-control"
                              />
                            </div>
                          </dd>
                        </dl>
                      </div>
                    </div>
                  </div>
                  <div className="panel">
                    <div className="grid-header">
                      <h2 className="grid-title">페이지 레이아웃</h2>
                      <div className="pos-right">
                        <a
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion02"
                          href="#collapse02-2"
                          className="btn-collapse collapsed"
                        />
                      </div>
                    </div>
                    <div className="panel-collapse collapse" id="collapse02-2">
                      <ul className="page-layout-list">
                        <li>
                          <a className="thum-grid-layout active">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-12">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-6">&nbsp;</td>
                                    <td className="col-6">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-12">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-12">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-6">&nbsp;</td>
                                    <td className="col-6">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-6">&nbsp;</td>
                                    <td className="col-6">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-12" colSpan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-12" colSpan="3">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4" colSpan="2">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4" colSpan="2">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-4" colSpan="2">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                        <li>
                          <a className="thum-grid-layout">
                            <div className="thum-grid-inner">
                              <div className="thum-grid-header"></div>
                              <table>
                                <tbody>
                                  <tr>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td className="col-4" colSpan="2">&nbsp;</td>
                                    <td className="col-4">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
