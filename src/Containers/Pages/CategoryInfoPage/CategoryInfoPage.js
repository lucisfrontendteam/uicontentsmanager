import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import $ from 'jquery';
import '../../../Resources/js/jquery.treegrid';
import _ from 'lodash';
import PathManager from '../../../Utils/PathManager';
import { v4 } from 'uuid';
import './custom.css';

export class CategoryInfoPage extends PureComponent {
  get pathManager() {
    const { props } = this;
    return new PathManager(props);
  }

  componentDidUpdate() {
    // const { props } = this;
    // props.categorylistmanagerreadInitAction(props);
    $('#treegrid').treegrid();
  }

  componentDidMount() {
    const { props } = this;
    props.categorylistmanagerreadInitAction(props);
    $('#treegrid').treegrid();
  }

  get flattedCategoryFilterTree() {
    const { props } = this;
    const { categoryFilterTree } = props.AdvencedFilterArgManagerState;
    let myIndex = 0;
    const flattenDatas = [];
    _.each(categoryFilterTree, categoryGroup => {
      myIndex += 1;
      const newCategoryGroup = {
        ...categoryGroup,
        myIndex,
      };
      flattenDatas.push(newCategoryGroup);
      _.each(categoryGroup.children, category => {
        myIndex += 1;
        const newCategory = {
          ...category,
          myIndex,
          parentIndex: newCategoryGroup.myIndex + 1,
        };
        flattenDatas.push(newCategory);
      });
    });
    return flattenDatas;
  }

  renderRecords() {
    const { props } = this;
    return _.map(props.CategoryListManagerState, node => {
      const {
        name,
        importantTerm = '',
        avoidTerm = '',
        parentIndex,
        myIndex,
      } = node;
      const mainClassName = `treegrid-${myIndex + 1}`;
      let parentClassName = '';
      if (parentIndex) {
        parentClassName = `treegrid-parent-${parentIndex}`;
      }
      const className = `${mainClassName} ${parentClassName}`;
      return (
        <tr className={className} key={v4()}>
          <td className="text-left">
            <span className="treegrid-title">{name}</span>
          </td>
          <th className="first">{importantTerm}</th>
          <th>{avoidTerm}</th>
        </tr>
      );
    });
  }

  render() {
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="btn-tab-area">
            <ul>
              <li>
                <Link
                  className="btn btn-tab-group"
                  to={this.pathManager.configCategoryEditPath}
                >카테고리 그룹</Link>
              </li>
              <li>
                <Link
                  className="btn btn-tab-info active"
                  to={this.pathManager.configCategoryInfoPath}
                >카테고리 용어</Link>
              </li>
            </ul>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="grid-wrap">
                <div className="grid-header">
                </div>
                <div className="grid-body ico-none">
                  <table className="list-table treegrid th-inbody" id="treegrid">
                    <colgroup>
                      <col style={{ width: '20%' }} />
                      <col style={{ width: '40%' }} />
                      <col style={{ width: '40%' }} />
                    </colgroup>
                    <tbody>
                      <tr className="treegrid-0">
                        <td className="text-left">
                          <span className="treegrid-title"></span>
                        </td>
                        <th className="first">주요 용어</th>
                        <th>회피 용어</th>
                      </tr>
                      {this.renderRecords()}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
