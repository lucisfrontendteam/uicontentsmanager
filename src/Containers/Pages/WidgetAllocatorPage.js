import React, { PureComponent } from 'react';

export class WidgetAllocatorPage extends PureComponent {
  render() {
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="row">
            <div className="col-md-3">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">시나리오</h2>
                </div>
                <div className="grid-body" id="tree1">
                </div>
              </div>
            </div>
            <div className="col-md-9">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">위젯 배치</h2>
                </div>
                <div className="grid-body">
                  <div className="widget-setting-wrap">
                    <div className="thum-header">
                      <div className="thum-logo-area">
                        <div className="thum-logo">
                          <label className="input-file-label">
                            <span className="btn-tool btn-plus">로고 업로드</span>
                            <div className="img-area" />
                            <input type="file" />
                          </label>
                        </div>
                        <div className="thum-logo2"></div>
                      </div>
                      <nav className="thum-gnb-area">
                        <div className="thum-top">고정 메뉴 영역</div>
                        <div className="thum-gnb">사용자 시나리오 메뉴 영역</div>
                      </nav>
                    </div>
                    <div className="thum-grid-wrap">
                      <div className="row">
                        <div className="col-md-6">
                          <div className="drop-area" data-num="a" />
                        </div>
                        <div className="col-md-6">
                          <div className="drop-area" data-num="b" />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="drop-area" data-num="c" />
                        </div>
                        <div className="col-md-6">
                          <div className="drop-area" data-num="d" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
