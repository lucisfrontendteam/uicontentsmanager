import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import $ from 'jquery';
import _ from 'lodash';
import { actions } from '../../../Redux';
import '../../../Resources/css/jquery-ui.css';
import '../../../Resources/css/jquery.timepicker.css';
import '../../../Resources/css/jquery.dataTables.css';
import '../../../Resources/css/jquery.mCustomScrollbar.css';
import '../../../Resources/css/jquery.jqtransform.css';
import '../../../Resources/css/jqtree.css';
import '../../../Resources/css/bootstrap.css';
import '../../../Resources/css/bootstrap-datepicker.standalone.css';
import '../../../Resources/css/admin-base.css';
import '../../../Resources/css/admin-custom.css';
import './custom.css';
import { NetworkSwitcher } from '../../../Components/NetworkSwitcher';
import UIContentsLoader from '../../../Utils/UIContentsLoader';
const { EnvironmentManager } = UIContentsLoader;
import environment from '../../../../config/environment';
import PathManager from '../../../Utils/PathManager';
import { parse } from 'query-string';

class LoginPage extends PureComponent {
  static defaultProps = {
    title: 'Content Manager',
  };

  constructor(props) {
    super(props);
    this.testLoginCount = 0;
    this.initOldIeSupport();
  }

  initOldIeSupport() {
    // const ie9TrapFound = document.getElementById('ie9Trap');
    // if (ie9TrapFound) {
    //   // eslint-disable-next-line
    //   require('../Resources/css/admin-ie9.css');
    // }

    const ie8TrapFound = document.getElementById('ie8Trap');
    if (ie8TrapFound) {
      // eslint-disable-next-line
      require('html5shiv');
      // eslint-disable-next-line
      require('../../../Resources/js/respond.min.js');
    }
  }

  get pathManager() {
    const { props } = this;
    return new PathManager(props);
  }

  get loginFailProps() {
    const { value: userId = '' } = document.getElementById('userId');
    const { value: userPass = '' } = document.getElementById('userPass');
    const newProps = {
      // 로그인 여부
      status: false,
      // 에러메세지 표시여부
      showError: true,
      id: userId,
      pass: userPass,
    };
    return newProps;
  }

  get loginProps() {
    const { value: userId = '' } = document.getElementById('userId');
    const { value: userPass = '' } = document.getElementById('userPass');
    const { props } = this;
    const newProps = {
      ...props,
      LoginManagerState: {
        userId,
        userPass,
        programId: 'manager',
      },
    };
    return newProps;
  }

  handleLogin(e) {
    e.preventDefault();
    e.stopPropagation();
    const { value: userId = '' } = document.getElementById('userId');
    const { value: userPass = '' } = document.getElementById('userPass');
    const userIdFilled = !!!_.isEmpty(userId);
    const userPassFilled = !!!_.isEmpty(userPass);
    const { props } = this;
    if (userIdFilled && userPassFilled) {
      // 로그인 개시
      props.loginmanagerloginInitAction(this.loginProps);
    } else {
      let newProps = {};
      if (!!!userIdFilled) {
        newProps = {
          ...this.loginFailProps,
          message: '아이디를 입력하십시오.',
        };
      } else if (!!!userPassFilled) {
        newProps = {
          ...this.loginFailProps,
          message: '패스워드를 입력하십시오.',
        };
      }
      props.loginmanagerInitAction(newProps);
    }
  }

  renderErrorMessage() {
    const { props } = this;
    const { showError, message } = props.LoginManagerState;
    let messageElement = '';
    if (showError) {
      messageElement = (<span>{message}</span>);
    }
    return messageElement;
  }

  // // 완전하지 않아서 일단 봉인
  // get formClassName() {
  //   const { props } = this;
  //   const { showError } = props.LoginManagerState;
  //   let failClassName = '';
  //   if (showError) {
  //     failClassName = 'fail';
  //   }
  //   const formClassName = `login-form ${failClassName}`;
  //   return formClassName;
  // }

  get isDistMode() {
    return process.env.DIST_TYPE === 'dist';
  }

  handleTestLogin() {
    if (!!!this.isDistMode) {
      this.testLoginCount += 1;
      if (this.testLoginCount >= 3) {
        $('#networkSwitch.glyphicon-earphone').click();
        $('#userId').val('root');
        $('#userPass').val('root');
      }
    }
  }

  get rootPath() {
    const { rootPath } = new EnvironmentManager(environment);
    return rootPath;
  }

  get projectSelectPath() {
    return `${this.rootPath}/projectSelect`;
  }

  manageHistory() {
    const { props } = this;
    const { ProjectState, LoginManagerState } = props;
    const projectNotFound = _.isEmpty(ProjectState);
    const loggedIn = LoginManagerState.status === true;
    if (projectNotFound) {
      if (loggedIn) {
        browserHistory.push(this.pathManager.projectSelectPath);
      }
    } else {
      if (loggedIn) {
        browserHistory.push(this.pathManager.firstScenarioPathInfo.path);
      }
    }
  }

  componentDidMount() {
    this.manageHistory();
  }

  renderNewWorkSwitcher(props) {
    let component = '';
    if (!!!this.isDistMode) {
      component = (
        <ul className="networkSwitcherContainer">
          <NetworkSwitcher {...props} />
        </ul>
      );
    }
    return component;
  }

  get queryStringObject() {
    const params = parse(location.search);
    return params || { userId: '' };
  }

  get userIdFromQuery() {
    const { userId } = this.queryStringObject;
    return _.isUndefined(userId) ? '' : userId;
  }

  render() {
    const { props } = this;
    return (
      <div className="wrapper">
        <div className="login-wrapper">
          {this.renderNewWorkSwitcher(props)}
          <div className="login-notice">
            {this.renderErrorMessage()}
          </div>
          <div id="shaker" className="login-form">
            <h1 onClick={() => this.handleTestLogin()}>{props.title}</h1>
            <div className="form-area">
              <form role="search" onSubmit={e => this.handleLogin(e)}>
                <ul className="clearfix">
                  <li>
                    <div className="pos-r">
                      <label className="admin-placeholder">ID</label>
                      <input
                        id="userId"
                        type="text"
                        placeholder="ID"
                        autoComplete="off"
                        defaultValue={this.userIdFromQuery}
                      />
                    </div>
                  </li>
                  <li>
                    <div className="pos-r">
                      <label className="admin-placeholder">PWD</label>
                      <input
                        id="userPass"
                        type="password"
                        placeholder="PWD"
                        autoComplete="off"
                      />
                    </div>
                  </li>
                </ul>
                <button id="go" type="submit" className="btn-submit-go">
                  GO
                </button>
              </form>
            </div>
          </div>
          <div className="login-bottom" />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
const connectedLoginPage = connect(mapStateToProps, actions)(LoginPage);
export { connectedLoginPage as LoginPage };
