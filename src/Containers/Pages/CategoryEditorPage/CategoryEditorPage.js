import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../../Redux';
import { Link } from 'react-router';
import $ from 'jquery';
import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/droppable';
import _ from 'lodash';
import 'bootstrap';
import '../../../Resources/js/notify';
import PathManager from '../../../Utils/PathManager';
import './custom.css';
import { NewCategoryNameModal } from './NewCategoryNameModal';
import CategoryNodeValidator from './CategoryNodeValidator';
import UIContents from '../../../Utils/UIContentsLoader';
const { PropsCreator } = UIContents;

class CategoryEditorPage extends PureComponent {
  get pathManager() {
    const { props } = this;
    return new PathManager(props);
  }

  handleEditAction(e) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    const id = $(e.target).data('id');
    const { value: categoryGroupName } = e.target;
    const categoryGroup = {
      id,
      name: categoryGroupName,
    };
    const validation
      = CategoryNodeValidator.validateGroupName(categoryGroupName, props);

    if (validation.errorFound) {
      $.notify(validation.message, 'error');
    } else {
      const newProps = PropsCreator.getCategoryGroupProps(props, categoryGroup);
      props.advencedfilterargmanagercategorygroupeditInitAction(newProps);
    }
  }

  initCategoryListDragAble() {
    const {
      selected_node: selectedNodeIds,
    } = this.$categoryList.tree('getState');
    const selectedNodes = _.map(selectedNodeIds, targetId => {
      const {
        id,
        name,
        type,
        avoidTerm,
        importantTerm,
      } = this.$categoryList.tree('getNodeById', targetId);
      return { id, name, type, avoidTerm, importantTerm };
    });
    const data = JSON.stringify(selectedNodes);
    $('#tree2 li.jqtree-selected').draggable({
      snap: '.ui-widget-header',
      snapMode: 'outer',
      cursorAt: { left: 50 },
      helper() {
        const proxyMarkup =
          `<span
            id="networkSwitch"
            class="glyphicon glyphicon-list"
            style="cursor: pointer;font-size: 50px;"
           />`;
        return $(proxyMarkup).data('categories', data);
      },
    });
  }

  destroyCategoryDragAble() {
    $('#tree2 li.jqtree-selected').draggable('destroy');
  }

  initCategoryGroupNameEnterEvent() {
    $(document).on('keyup', 'input.editTextBox', e => {
      if (e.keyCode === 13) {
        this.handleEditAction(e);
      }
    });
  }

  destroyCategoryGroupNameEnterEvent() {
    $(document).off('keyup', 'input.editTextBox');
  }

  initCategoryGroupDroppable() {
    const { props } = this;
    $('#tree1 div.jqtree-element').droppable({
      drop(e, ui) {
        const categories = JSON.parse($(ui.helper).data('categories'));
        const $target = $(e.target);
        const { id, type } = $target.data();
        let groupId = id;
        let position = 0;
        if (type === 'category') {
          const { id: parentId } = $target.parents('li.jqtree-folder').data('node');
          groupId = parentId;
          position = $target.index() + 1;
        }
        const newPros = PropsCreator.getAddtionalProps(props);
        props.advencedfilterargmanagercategorymoveInitAction(
          newPros,
          groupId,
          categories,
          position
        );
      },
    });
  }

  initEventHandlers() {
    this.initCategoryGroupNameEnterEvent();
  }

  destroyEventHandlers() {
    this.destroyCategoryGroupNameEnterEvent();
    this.destroyCategoryDragAble();
  }

  initCategoryTree() {
    const self = this;
    const { props } = self;
    const { categoryFilterTree } = props.AdvencedFilterArgManagerState;
    this.$categoryTree = $('#tree1').tree({
      data: categoryFilterTree || [],
      autoOpen: true,
      buttonLeft: false,
      dragAndDrop: true,
      selectable: true,
      useContextMenu: true,
      onCreateLi: (node, $li) => {
        const { editMode, name, id, type } = node;
        if (editMode) {
          const template
            = `<input type="text" data-id="${id}" value="${name}" class="editTextBox" />`;
          $li.find('span').html(template);
        }
        $li.find('> div').data({ id, type });
      },
      onCanMoveTo(movedNode, targetNode, position) {
        // const isGroupToGroupMoving =
        //   movedNode.type === 'group' && targetNode.type === 'group' && position !== 'inside';
        const isCategoryNearCategoryMoving =
          movedNode.type === 'category' && targetNode.type === 'category' && position !== 'inside';
        const isCategoryToGroupMoving =
          movedNode.type === 'category' && targetNode.type === 'group' && position === 'inside';
        return isCategoryNearCategoryMoving || isCategoryToGroupMoving;
      },
    })
    .bind('tree.click', e => this.handleTreeNodeClick(e))
    .bind('tree.contextmenu', e => this.handleContextMenuCalled(e))
    .bind('tree.move', e => this.handleTreeNodeMoved(e));
  }

  handleTreeNodeClick(e) {
    e.preventDefault();
    const selectedNode = e.node;
    if (this.$categoryTree.tree('isNodeSelected', selectedNode)) {
      if (selectedNode.type === 'group') {
        this.$categoryTree.tree('removeFromSelection', selectedNode);
        _.each(selectedNode.children, child => {
          this.$categoryTree.tree('removeFromSelection', child);
        });
      } else {
        this.$categoryTree.tree('removeFromSelection', selectedNode.parent);
        this.$categoryTree.tree('removeFromSelection', selectedNode);
      }
    } else {
      this.$categoryTree.tree('addToSelection', selectedNode);
      _.each(selectedNode.children, child => {
        this.$categoryTree.tree('addToSelection', child);
      });
    }
  }

  handleContextMenuCalled(e) {
    const { props } = this;
    const { type, editMode, id } = e.node;
    if (type === 'group' && editMode !== true) {
      props.setCategoryGroupEditMode(id);
    }
  }

  handleTreeNodeMoved(e) {
    e.preventDefault();
    const {
      target_node: targetNode,
      moved_node: movedNode,
      position: positionType,
      previous_parent: previousParent,
    } = e.move_info;
    const {
      id, name, type, children,
    } = movedNode;
    const categories = [{ id, name, type, children }];
    const groupNode
      = _.isUndefined(targetNode.parent.type) ? targetNode : targetNode.parent;
    const { id: groupId } = groupNode;

    let position = 0;
    _.each(groupNode.children, (child, idx) => {
      if (_.isEqual(child.id, targetNode.id)) {
        position = idx;
        return;
      }
    });
    switch (positionType) {
      case 'before': {
        position -= 1;
        if (position < 0) {
          position = 0;
        }
      } break;

      case 'after': {
        position += 1;
        const { length: childenLength } = groupNode.children;
        if (position > childenLength + 1) {
          position = childenLength + 1;
        }
      } break;

      case 'inside':
      default: {
        // do nothing
      } break;
    }
    // 같은 그룹 내에서의 이동을 금지함
    if (groupId !== previousParent.id) {
      const { props } = this;
      const newPros = PropsCreator.getAddtionalProps(props);
      props.advencedfilterargmanagercategorymoveInitAction(
        newPros,
        groupId,
        categories,
        position,
        previousParent.id
      );
    }
  }

  initCategoryList() {
    const self = this;
    const { props } = self;
    this.$categoryList = $('#tree2').tree({
      data: props.CategoryListManagerState || [],
      autoOpen: true,
      buttonLeft: false,
      selectable: true,
      useContextMenu: true,
      // onCreateLi(node, $li) {
      //   const { name, avoidTerm, importantTerm } = node;
      //   const first = name;
      //   const second = _.isNull(avoidTerm) ? '' : ` | 회피 용어 : ${avoidTerm}`;
      //   const third = _.isNull(importantTerm) ? '' : ` | 주요 용어 : ${importantTerm}`;
      //   $li.find('span').html(`${first}${second}${third}`);
      // },
    }).bind('tree.click', e => {
      e.preventDefault();
      const selectedNode = e.node;
      if (this.$categoryList.tree('isNodeSelected', selectedNode)) {
        this.$categoryList.tree('removeFromSelection', selectedNode);
      } else {
        this.$categoryList.tree('addToSelection', selectedNode);
      }
      this.initCategoryListDragAble();
    });
  }

  initTrees() {
    this.initCategoryTree();
    this.initCategoryList();
  }

  destroyTrees() {
    const { $categoryTree, $categoryList } = this;
    if ($categoryTree) {
      $categoryTree.tree('destroy');
    }
    if ($categoryList) {
      $categoryList.tree('destroy');
    }
  }

  handleAddCategoryGroup() {
    $('#NewCategoryNameModal').modal('show');
  }

  handleRemoveNodes() {
    const validation =
      CategoryNodeValidator.validateRemoveNode(this.$categoryTree);
    if (validation.errorFound) {
      $.notify(validation.message, 'error');
    } else {
      const {
        selected_node: selectedNodeIds,
      } = this.$categoryTree.tree('getState');

      const removeNodes = _.map(selectedNodeIds,
        nodeId => this.$categoryTree.tree('getNodeById', nodeId));

      const reorderedNodes =
        _.orderBy(removeNodes, 'type', 'asc');

      const removeOnNetworkNodes = _.map(selectedNodeIds, nodeId => {
        const { id, group, type, parent } = this.$categoryTree.tree('getNodeById', nodeId);
        let nodeForNetwork = {};
        if (parent) {
          const { id: parentId } = parent;
          nodeForNetwork = {
            id,
            group,
            type,
            parentId,
          };
        } else {
          nodeForNetwork = {
            id,
            group,
            type,
          };
        }
        return nodeForNetwork;
      });

      _.each(reorderedNodes, targetNode => {
        this.$categoryTree.tree('removeNode', targetNode);
      });

      const stateRemoveAfter = JSON.parse(this.$categoryTree.tree('toJson'));
      const { props } = this;
      const newProps
        = PropsCreator.getCategoryRemoveProps(props, removeOnNetworkNodes, stateRemoveAfter);
      props.advencedfilterargmanagercategoryremoveInitAction(newProps);
    }
  }

  handleCategoryRefresh() {
    const { props } = this;
    props.categorylistmanagerreadInitAction(props);
    $.notify('카테고리 리스트가 리프레쉬 되었습니다.', 'info');
  }

  handleCategorySearch(e) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    const { value } = document.getElementById('groupNameFilter');
    props.categorySearchKeywordInitAction(value);
  }

  componentDidUpdate() {
    this.destroyTrees();
    this.initTrees();
    this.initCategoryGroupDroppable();
  }

  componentDidMount() {
    const { props } = this;
    props.advencedfilterargmanagerreadInitAction(props, true);
    props.categorylistmanagerreadInitAction(props);
    this.initEventHandlers();
  }

  componentWillUnmount() {
    this.destroyEventHandlers();
  }

  render() {
    const { props } = this;
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="btn-tab-area">
            <ul>
              <li>
                <Link
                  className="btn btn-tab-group active"
                  to={this.pathManager.configCategoryEditPath}
                >카테고리 그룹</Link>
              </li>
              <li>
                <Link
                  className="btn btn-tab-info"
                  to={this.pathManager.configCategoryInfoPath}
                >카테고리 용어</Link>
              </li>
            </ul>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="grid-wrap">
                <div className="grid-header">
                  <div className="pos-right">
                    <button
                      type="button"
                      className="btn-tool btn-plus"
                      onClick={() => this.handleAddCategoryGroup()}
                    >카테고리 그룹 추가</button>
                    <button
                      type="button"
                      className="btn-tool btn-minus"
                      onClick={() => this.handleRemoveNodes()}
                    >선택 삭제</button>
                  </div>
                </div>
                <div className="grid-body ico-checkbox" id="tree1" />
              </div>
            </div>
            <div className="col-md-6">
              <div className="grid-wrap">
                <div className="grid-header">
                  <div className="abs">
                    <form
                      role="search"
                      className="rela"
                      onSubmit={(e) => this.handleCategorySearch(e)}
                    >
                      <input
                        type="text"
                        id="groupNameFilter"
                        className="form-control"
                        autoComplete="off"
                      />
                      <button type="button">
                        <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
                      </button>
                    </form>
                  </div>
                  <div className="pos-right">
                    <button
                      type="button"
                      className="btn-tool btn-reset"
                      onClick={() => this.handleCategoryRefresh()}
                    >리프레쉬</button>
                  </div>
                </div>
                <div className="grid-body ico-none" id="tree2" />
              </div>
            </div>
          </div>
        </section>
        <NewCategoryNameModal {...props} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    CategorySearchKeywordState,
    CategoryListManagerState,
  } = state;

  let newState = {};
  if (!!!_.isEmpty(CategorySearchKeywordState)) {
    const newCategoryListManagerState
      = _.filter(CategoryListManagerState,
        category => category.name.indexOf(CategorySearchKeywordState) !== -1);
    newState = {
      ...state,
      CategoryListManagerState: newCategoryListManagerState,
    };
  } else {
    newState = { ...state };
  }
  return newState;
};
const connectedCategoryEditorPage
  = connect(mapStateToProps, actions)(CategoryEditorPage);
export { connectedCategoryEditorPage as CategoryEditorPage };
