import _ from 'lodash';

export default class CategoryNodeValidator {
  static validateGroupName(categoryGroupName, props) {
    const empty = _.isEmpty(categoryGroupName);
    let duplicated = false;
    let message = '';
    if (empty) {
      message = '카테고리 그룹의 이름을 입력하여 주십시오';
    } else {
      const { categoryFilterTree } = props.AdvencedFilterArgManagerState;
      const duplicatedNode
        = _.find(categoryFilterTree, filter => filter.name === categoryGroupName);
      duplicated = !!!_.isUndefined(duplicatedNode);

      if (duplicated) {
        message = `${categoryGroupName} 은(는) 이미 사용되고 있습니다.`;
      }
    }
    return {
      errorFound: empty || duplicated,
      message,
    };
  }

  static validateRemoveNode($targetTree) {
    const {
      selected_node: selectedNodeIds,
    } = $targetTree.tree('getState');
    const empty = _.isEmpty(selectedNodeIds);
    let message = '';
    if (empty) {
      message = '삭제할 노드를 선택하여 주십시오';
    }
    return {
      errorFound: empty,
      message,
    };
  }
}
