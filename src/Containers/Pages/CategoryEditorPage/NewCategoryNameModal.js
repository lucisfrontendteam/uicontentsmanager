import React, { PureComponent } from 'react';
import $ from 'jquery';
import 'bootstrap';
import '../../../Resources/js/notify';
import CategoryNodeValidator from './CategoryNodeValidator';
import UIContents from '../../../Utils/UIContentsLoader';
const { PropsCreator } = UIContents;

export class NewCategoryNameModal extends PureComponent {
  static defaultProps = {
    inputId: 'newGroupName',
  };

  handleInputNewCategoryName(e) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    const { value: newCategoryGroupName }
      = document.getElementById(props.inputId);
    const validation
      = CategoryNodeValidator.validateGroupName(newCategoryGroupName, props);

    if (validation.errorFound) {
      $.notify(validation.message, 'error');
    } else {
      const categoryGroup = {
        id: '',
        name: newCategoryGroupName,
      };
      const newProps = PropsCreator.getCategoryGroupProps(props, categoryGroup);
      props.advencedfilterargmanagercategorygroupaddInitAction(newProps);
      $('#NewCategoryNameModal').modal('hide');
      document.getElementById(props.inputId).value = '';
    }
  }

  render() {
    const { props } = this;
    return (
      <div
        className="modal fade bd-example-modal-sm"
        tabIndex="-1" role="dialog"
        aria-hidden="true"
        id="NewCategoryNameModal"
      >
        <div className="modal-dialog modal-md">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title" id="myModalLabel">새로운 카테고리 그룹의 이름을 입력하세요</h4>
            </div>
            <div className="modal-body">
              <form role="search" onSubmit={(e) => this.handleInputNewCategoryName(e)}>
                <div className="form-group">
                  <label
                    htmlFor={props.inputId}
                    className="form-control-label"
                  >새로운 카테고리 그룹의 이름:</label>
                  <input
                    type="text"
                    className="form-control"
                    id={props.inputId}
                    autoComplete="false"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
