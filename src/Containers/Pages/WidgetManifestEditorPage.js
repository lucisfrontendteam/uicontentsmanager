import React, { PureComponent } from 'react';
import widget01 from '../../Resources/images/@widget_01.jpg';

export class WidgetManifestEditorPage extends PureComponent {
  render() {
    return (
      <div className="content-wrapper">
        <section className="content-inner">
          <div className="row">
            <div className="col-md-3">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-input">
                    <div className="pos-r">
                      <label className="admin-placeholder">search...</label>
                      <input type="text" placeholder="search..." className="form-control" />
                    </div>
                  </h2>
                  <div className="pos-right">
                    <button type="button" className="btn-tool btn-search"></button>
                  </div>
                </div>
                <div className="grid-body">
                  <ul className="widget-list-wrap">
                    <li className="active">
                      <div className="widget-thumbnail-area">
                        <img src={widget01} alt="" />
                      </div>
                      <div className="summary-info-area">
                        <dl>
                          <dt>위젯명위젯명위젯명</dt>
                          <dd>영문명: contentcontentcontent</dd>
                          <dd>사이즈: 4x4</dd>
                          <dd>태그: First,First,First,First</dd>
                        </dl>
                      </div>
                    </li>
                    <li>
                      <div className="widget-thumbnail-area"></div>
                      <div className="summary-info-area">
                        <dl>
                          <dt>위젯명</dt>
                          <dd>영문명: content</dd>
                          <dd>사이즈: 4x4</dd>
                          <dd>태그: First</dd>
                        </dl>
                      </div>
                    </li>
                    <li>
                      <div className="widget-thumbnail-area"></div>
                      <div className="summary-info-area">
                        <dl>
                          <dt>위젯명</dt>
                          <dd>영문명: content</dd>
                          <dd>사이즈: 4x4</dd>
                          <dd>태그: First</dd>
                        </dl>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-9">
              <div className="grid-wrap">
                <div className="grid-header">
                  <h2 className="grid-title">위젯명 / 정보</h2>
                  <div className="pos-right">
                    <button
                      type="button"
                      className="btn-tool btn-edit"
                      data-toggle="modal"
                      onClick="$('#widgetAddLayer').modal('show')"
                    >위젯 수정</button>
                    <button
                      type="button"
                      className="btn-tool btn-plus"
                      data-toggle="modal"
                      onClick="$('#widgetAddLayer').modal('show')"
                    >위젯 추가</button>
                  </div>
                </div>
                <div className="grid-body">
                  <div className="widget-detail-view">
                    <div className="widget-image-area">
                      <img src={widget01} alt="" />
                    </div>
                    <table className="widget-detail-info">
                      <tbody>
                        <tr>
                          <th>위젯 영문명</th>
                          <td>Content</td>
                        </tr>
                        <tr>
                          <th>위젯 사이즈</th>
                          <td>4x4</td>
                        </tr>
                        <tr>
                          <th>위젯 태그</th>
                          <td>First</td>
                        </tr>
                        <tr>
                          <th>제공</th>
                          <td>루키스 기업부설 연구소 (2016-11-20 16:00)</td>
                        </tr>
                        <tr>
                          <th>태그</th>
                          <td>성과,볼륨...</td>
                        </tr>
                        <tr>
                          <th>URL</th>
                          <td>
                            <a
                              href="http://www.somesite.com/widget/234/default"
                              target="_blank"
                            >http://www.somesite.com/widget/234/default</a>
                          </td>
                        </tr>
                        <tr>
                          <th>Config</th>
                          <td>
                            <a
                              href="https://www.somesite.com/widget/234/config"
                              target="_blank"
                            >https://www.somesite.com/widget/234/config</a>
                          </td>
                        </tr>
                        <tr>
                          <th>Search</th>
                          <td>
                            <a
                              href="https://www.somesite.com/widget/234/load"
                              target="_blank"
                            >https://www.somesite.com/widget/234/load</a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
