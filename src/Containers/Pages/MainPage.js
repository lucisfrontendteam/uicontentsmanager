import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../Redux';
import { GnbPlayer } from '../../Components/GnbPlayer';

class MainPage extends PureComponent {
  componentDidMount() {
    const { props } = this;
    props.advencedfilterargmanagerreadInitAction(props, true);
  }

  render() {
    const { props } = this;
    const { contents } = props;
    const TargetComponent = React.cloneElement(contents, props);
    return (
      <div className="wrapper">
        <div className="main-bg">
          <span className="lt"></span>
          <span className="lb"></span>
          <span className="rt"></span>
          <span className="rb"></span>
        </div>
        <GnbPlayer {...props} />
        {TargetComponent}
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
const connectedMainPage
  = connect(mapStateToProps, actions)(MainPage);
export { connectedMainPage as MainPage };
