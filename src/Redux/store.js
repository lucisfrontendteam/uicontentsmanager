import {
  createStore,
  applyMiddleware,
  compose,
} from 'redux';
import devTools from 'remote-redux-devtools';
import thunkMiddleWare from 'redux-thunk';
import reducers from './reducers';
import { actions } from './actions';
import UIContentsLoader from '../Utils/UIContentsLoader';
const { EnvironmentManager } = UIContentsLoader;

/* eslint-disable no-underscore-dangle */
class Store {
  constructor(
    reducers_,
    createStore_,
    applyMiddleware_,
    compose_,
    thunkMiddleWare_,
    initActions_
  ) {
    this._reducers = reducers_;
    this._createStore = createStore_;
    this._applyMiddleware = applyMiddleware_;
    this._compose = compose_;
    this._thunkMiddleWare = thunkMiddleWare_;
    this._initActions = initActions_;
    this.init();
  }

  createFinalStore() {
    const {
      _reducers,
      _createStore,
      _applyMiddleware,
      _compose,
      _thunkMiddleWare,
    } = this;

    const finalCreateStore =
      _compose(_applyMiddleware(_thunkMiddleWare), this.applyReduxDevTool())(_createStore);
    this.store = finalCreateStore(_reducers);
  }

  init() {
    this.createFinalStore();
    this.dispatchInitActions();
  }

  dispatchInitActions() {
    this._initActions.forEach(action => this.store.dispatch(action()));
  }

  applyReduxDevTool() {
    const isDevEnvironment = EnvironmentManager.isDevelopmentEnv();
    const isClient = typeof window === 'object';
    const reduxExtensionFound = typeof window.devToolsExtension !== 'undefined';
    return (isDevEnvironment && isClient && reduxExtensionFound) ?
      window.devToolsExtension() : f => f;
  }

  applyReduxRemoteDevTool() {
    const isDevEnvironment = EnvironmentManager.isDevelopmentEnv();
    return isDevEnvironment ? devTools() : f => f;
  }
}

const initActions = [
  actions.widgetCoreListInitAction,
  actions.widgetThemeListInitAction,
  actions.widgetViewerThemeListInitAction,
  actions.projectThemeListInitAction,
  actions.menuTreeInitAction,
  actions.networkModeOnAction,
  actions.loginmanagerInitAction,
];

export const { store } =
  new Store(
    reducers,
    createStore,
    applyMiddleware,
    compose,
    thunkMiddleWare,
    initActions
  );
