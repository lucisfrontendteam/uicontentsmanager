import _ from 'lodash';
import * as actionTypes from './actionTypes';
import DefaultStates from './DefaultStates';
const defaultStates = new DefaultStates();
import UIContentsLoader from '../Utils/UIContentsLoader';
const {
  WidgetRefiner,
  WidgetCores,
  CommonManagerActionList,
} = UIContentsLoader;
const { widgetActionList } = new WidgetRefiner(WidgetCores);
import {
  NetworkSwitcherActions,
} from '../Components/NetworkSwitcher';

function menuTreeInitAction(menuTreeState = defaultStates.menuTreeState) {
  return {
    type: actionTypes.MENU_TREE_INIT,
    payload: { menuTreeState },
  };
}

function menuNodeSelectByUriAction(menuUri) {
  return {
    type: actionTypes.MENU_NODE_SELECT,
    payload: { menuUri },
  };
}

function categorySearchKeywordInitAction(keyword) {
  return {
    type: actionTypes.CATEGORY_SEARCH_KEYWORD_INIT,
    payload: { keyword },
  };
}

function widgetCoreListInitAction(widgetCoreListState = defaultStates.widgetCoreListState) {
  return {
    type: actionTypes.WIDGET_CORE_LIST_INIT,
    payload: { widgetCoreListState },
  };
}

function widgetThemeListInitAction(widgetThemeListState = defaultStates.widgetThemeState) {
  return {
    type: actionTypes.WIDGET_THEME_LIST_INIT,
    payload: { widgetThemeListState },
  };
}

function widgetViewerThemeListInitAction(
  widgetViewerThemeListState = defaultStates.widgetViewerThemeListState
) {
  return {
    type: actionTypes.WIDGET_VIEWER_THEME_LIST_INIT,
    payload: { widgetViewerThemeListState },
  };
}

function projectThemeListInitAction(
  projectThemeListState = defaultStates.projectThemeListState,
) {
  return {
    type: actionTypes.PROJECT_THEME_LIST_INIT,
    payload: { projectThemeListState },
  };
}

function projectInitAction(projectId, props) {
  const { ProjectManagerState } = props;
  const selectedProject =
    _.find(ProjectManagerState, projectState => projectState.id === projectId);
  return {
    type: actionTypes.PROJECT_INIT,
    payload: { projectState: selectedProject },
  };
}

export function userFilterInitAction(keyword) {
  return {
    type: actionTypes.USER_FILTER_INIT,
    payload: { keyword },
  };
}

export const actions = {
  ...widgetActionList,
  ...CommonManagerActionList,
  ...NetworkSwitcherActions,
  menuTreeInitAction,
  menuNodeSelectByUriAction,
  widgetCoreListInitAction,
  widgetThemeListInitAction,
  widgetViewerThemeListInitAction,
  projectThemeListInitAction,
  projectInitAction,
  categorySearchKeywordInitAction,
  userFilterInitAction,
};
