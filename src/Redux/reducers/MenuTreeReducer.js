import _ from 'lodash';
import * as actionTypes from '../actionTypes';
import UIContents from '../../Utils/UIContentsLoader';
const { TreeTraveser } = UIContents;

export default function MenuTreeState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.MENU_TREE_INIT: {
      const { menuTreeState } = action.payload;
      finalState = { ...menuTreeState };
    } break;

    case actionTypes.MENU_NODE_SELECT: {
      const { menuUri } = action.payload;
      const treeTraveser = new TreeTraveser(state);
      const rootNode = treeTraveser.getRootNode();
      _.map(rootNode.children, depth1Node => {
        _.map(depth1Node.children, depth2Node => {
          depth2Node.walk(node => {
            const isTarget = (node.model.uri === menuUri);
            treeTraveser.setNode(node, { selected: isTarget });
          });
        });
      });
      finalState = { ...treeTraveser.treeData };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
