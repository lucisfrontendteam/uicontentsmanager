import * as actionTypes from '../actionTypes';
import UIContents from '../../Utils/UIContentsLoader';
const { ProjectWriter, CommonManagerActionTypeList } = UIContents;

/**
 * 시나리오 트리용 액션별 페이로드를 관리하는 리듀서를 생성하는 함수.
 *
 * @param state
 * @param action
 * @returns {Array}
 * @constructor
 */
export default function ProjectState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case CommonManagerActionTypeList.LOGOUT: {
      finalState = {};
    } break;

    case CommonManagerActionTypeList.PROJECT_REMOVE: {
      const { widgetState } = action.payload;
      const { projectId } = widgetState;
      if (projectId && state.id === projectId) {
        finalState = {};
      }
    } break;

    case CommonManagerActionTypeList.PROJECT_EDIT_START:
    case CommonManagerActionTypeList.PROJECT_EDIT_END: {
      const { isEditMode } = action.payload;
      finalState = { ...state, isEditMode };
    } break;

    case CommonManagerActionTypeList.PROJECT_THUMBNAIL_UPLOAD: {
      const { widgetState } = action.payload;
      const { thumbnail } = widgetState;
      finalState = { ...state, thumbnail };
    } break;

    case CommonManagerActionTypeList.PROJECT_CI_UPLOAD: {
      const { widgetState } = action.payload;
      const { ci } = widgetState;
      finalState = { ...state, ci };
    } break;

    case actionTypes.PROJECT_INIT: {
      const { projectState } = action.payload;
      finalState = { ...projectState };
    } break;

    case actionTypes.SCENARIO_SELECT: {
      const { scenarioUri } = action.payload;
      const props = { ProjectState: state };
      const newProjectState = ProjectWriter.selectScenarioByUri(props, scenarioUri);
      finalState = { ...newProjectState, isEditMode: false };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
