import * as actionTypes from '../actionTypes';

/**
 * 위젯 리스트용 액션별 페이로드를 관리하는 리듀서를 생성하는 함수.
 *
 * @param state
 * @param action
 * @returns {Array}
 * @constructor
 */
export default function WidgetCoreListState(states = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.WIDGET_CORE_LIST_INIT: {
      const { widgetCoreListState } = action.payload;
      finalState = finalState.concat(widgetCoreListState);
    } break;

    default: {
      finalState = finalState.concat(states);
    }
  }

  return finalState;
}
