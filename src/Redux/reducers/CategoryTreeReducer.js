import UIContents from '../../Utils/UIContentsLoader';
const { CommonManagerActionTypeList } = UIContents;

export default function CategoryTreeState(state = [], action = null) {
  let finalState = {};
  switch (action.type) {
    case CommonManagerActionTypeList.ADVENCED_FILTER_ARG_STATE_INIT: {
      const { widgetState } = action.payload;
      const {
        AdvencedFilterArgManagerState = {
          categoryFilterTree: [],
        },
      } = widgetState;
      const targetState = AdvencedFilterArgManagerState.categoryFilterTree;
      finalState = [...targetState];
    } break;

    default: {
      finalState = [...state];
    }
  }

  return finalState;
}
