import * as actionTypes from '../actionTypes';

export default function UserFilterState(state = '', action = null) {
  let finalState = '';
  switch (action.type) {
    case actionTypes.USER_FILTER_INIT: {
      const { payload } = action;
      finalState = payload.keyword;
    } break;

    default: {
      finalState = state;
    }
  }

  return finalState;
}
