import * as actionTypes from '../actionTypes';

export default function CategorySearchKeywordState(state = '', action = null) {
  let finalState = '';
  switch (action.type) {
    case actionTypes.CATEGORY_SEARCH_KEYWORD_INIT: {
      const { keyword } = action.payload;
      finalState = keyword;
    } break;

    default: {
      finalState = state;
    }
  }

  return finalState;
}
