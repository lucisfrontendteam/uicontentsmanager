import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import WidgetCoreListState from './WidgetCoreListReducer';
import WidgetThemeListState from './WidgetThemeListReducer';
import WidgetViewerLayoutListState from './WidgetViewerLayoutListReducer';
import ProjectState from './ProjectReducer';
import ProjectThemeListState from './ProjectThemeListReducer';
import MenuTreeState from './MenuTreeReducer';
import CategorySearchKeywordState from './CategorySearchKeywordReducer';
import UserFilterState from './UserFilterReducer';
import UIContentsLoader from '../../Utils/UIContentsLoader';
const {
  WidgetRefiner,
  WidgetCores,
  CommonManagerReducers,
} = UIContentsLoader;
const { widgetReducers } = new WidgetRefiner(WidgetCores);
import { NetworkSwitcherReducer } from '../../Components/NetworkSwitcher';

export const dashboardReducers = {
  ...widgetReducers,
  ...NetworkSwitcherReducer,
  ...MenuTreeState,
  ...CommonManagerReducers,
  CategorySearchKeywordState,
  WidgetCoreListState,
  WidgetThemeListState,
  WidgetViewerLayoutListState,
  ProjectState,
  ProjectThemeListState,
  MenuTreeState,
  UserFilterState,
  routing: routerReducer,
};

export default combineReducers(dashboardReducers);
