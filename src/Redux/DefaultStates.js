import _ from 'lodash';
import UIContents from '../Utils/UIContentsLoader';
const {
  WidgetCores,
  WidgetThemes,
  WidgetViewerLayouts,
  WidgetRefiner,
  ProjectManagerReadSampleDatas,
} = UIContents;
const { widgetCores } = new WidgetRefiner(WidgetCores);
import PathManager from '../Utils/PathManager';

export default class DefaultStates {
  static queryKey = 'advencedFilter';
  static emptyOption = { id: 0, name: '선택' };
  static zeroTime = '0000';

  get pathManager() {
    return new PathManager();
  }

  get widgetCoreListState() {
    return _.map(widgetCores, widgetCore => {
      const { displayName, element } = widgetCore;
      const { name, type } = element.defaultProps;
      return {
        constructor: element,
        displayName,
        name,
        type,
      };
    });
  }

  get widgetThemeState() {
    return _.map(Object.keys(WidgetThemes), displayName => {
      const constructor = WidgetThemes[displayName];
      const { name, type } = constructor.defaultProps;
      return {
        constructor,
        displayName,
        name,
        type,
      };
    });
  }

  get widgetViewerThemeListState() {
    return _.map(Object.keys(WidgetViewerLayouts), displayName => {
      const constructor = WidgetViewerLayouts[displayName];
      const { name, type } = constructor.defaultProps;
      return {
        constructor,
        displayName,
        name,
        type,
      };
    });
  }

  get ProjectManagerState() {
    return ProjectManagerReadSampleDatas;
  }

  get menuTreeState() {
    return {
      accessPath: this.pathManager.rootUri,
      id: 'menus',
      children: [
        {
          id: this.pathManager.homeRootUri,
          name: '홈',
          uri: this.pathManager.homeRootUri,
          children: [
            {
              id: this.pathManager.projectSelectUri,
              name: '프로젝트 선택',
              uri: this.pathManager.projectSelectUri,
              selected: true,
            },
          ],
        },
        // {
        //   id: this.pathManager.userScenarioRootUri,
        //   name: '시나리오',
        //   uri: this.pathManager.userScenarioRootUri,
        //   children: [
        //     {
        //       id: this.pathManager.userScenarioEditUri,
        //       name: '시나리오 편집',
        //       uri: this.pathManager.userScenarioEditUri,
        //     },
        //     {
        //       id: this.pathManager.userScenarioAllocateUri,
        //       name: '위젯 배치',
        //       uri: this.pathManager.userScenarioAllocateUri,
        //     },
        //     {
        //       id: this.pathManager.userScenarioPermissionUri,
        //       name: '권한 설정',
        //       uri: this.pathManager.userScenarioPermissionUri,
        //     },
        //   ],
        // },
        // {
        //   id: this.pathManager.widgetRootUri,
        //   name: '위젯',
        //   uri: this.pathManager.widgetRootUri,
        //   children: [
        //     {
        //       id: this.pathManager.widgetManifestUri,
        //       name: '위젯 설정',
        //       uri: this.pathManager.widgetManifestUri,
        //     },
        //   ],
        // },
        {
          id: this.pathManager.configRootUri,
          name: '설정',
          uri: this.pathManager.configRootUri,
          children: [
            {
              id: this.pathManager.configPermissionUri,
              name: '사용자 권한 설정',
              uri: this.pathManager.configPermissionUri,
            },
            {
              id: this.pathManager.configCategoryEditUri,
              name: '카테고리 그룹 편집',
              uri: this.pathManager.configCategoryEditUri,
            },
            {
              id: this.pathManager.configCategoryInfoUri,
              name: '카테고리 용어 확인',
              uri: this.pathManager.configCategoryInfoUri,
            },
          ],
        },
      ],
    };
  }
}
