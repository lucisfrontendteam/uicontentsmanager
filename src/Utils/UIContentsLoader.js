/* eslint-disable */
if (process.env.NODE_ENV === 'development') {
  module.exports = require('../../../resourcecreator/src/BuildEntry');
} else {
  const UIContents = window.UIContents;
  module.exports = UIContents.default;
}