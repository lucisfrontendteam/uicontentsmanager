import 'babel-polyfill';
import UIContentsLoader from './UIContentsLoader';
const { EnvironmentManager } = UIContentsLoader;
import environment from '../../config/environment';

export default class PathManager {
  get rootUri() {
    const { rootPath } = new EnvironmentManager(environment);
    const [, ...uri] = rootPath;
    return uri.join('');
  }

  get rootPath() {
    return `/${this.rootUri}`;
  }

  get loginPath() {
    const { loginPath } = new EnvironmentManager(environment);
    return loginPath;
  }

  get errorPath() {
    const { errorPath } = new EnvironmentManager(environment);
    return errorPath;
  }

  get homeRootUri() {
    return 'home';
  }

  get homeRootPath() {
    return `${this.rootPath}/${this.homeRootUri}`;
  }

  get projectSelectUri() {
    return 'projectSelect';
  }

  get projectSelectPath() {
    return `${this.homeRootPath}/${this.projectSelectUri}`;
  }

  get userScenarioRootUri() {
    return 'userScenario';
  }

  get userScenarioRootPath() {
    return `${this.rootPath}/${this.userScenarioRootUri}`;
  }

  get userScenarioEditUri() {
    return 'edit';
  }

  get userScenarioEditPath() {
    return `${this.userScenarioRootPath}/${this.userScenarioEditUri}`;
  }

  get userScenarioAllocateUri() {
    return 'allocate';
  }

  get userScenarioAllocatePath() {
    return `${this.userScenarioRootPath}/${this.userScenarioAllocateUri}`;
  }

  get userScenarioPermissionUri() {
    return 'permission';
  }

  get userScenarioPermissionPath() {
    return `${this.userScenarioRootPath}/${this.userScenarioPermissionUri}`;
  }

  get widgetRootUri() {
    return 'widget';
  }

  get widgetRootPath() {
    return `${this.rootPath}/${this.widgetRootUri}`;
  }

  get widgetManifestUri() {
    return 'manifest';
  }

  get widgetManifestPath() {
    return `${this.widgetRootPath}/${this.widgetManifestUri}`;
  }

  get configRootUri() {
    return 'config';
  }

  get configRootPath() {
    return `${this.rootPath}/${this.configRootUri}`;
  }

  get configCategoryEditUri() {
    return 'categoryEdit';
  }

  get configCategoryEditPath() {
    return `${this.configRootPath}/${this.configCategoryEditUri}`;
  }

  get configCategoryInfoUri() {
    return 'categoryInfo';
  }

  get configCategoryInfoPath() {
    return `${this.configRootPath}/${this.configCategoryInfoUri}`;
  }

  get configCounselorUri() {
    return 'counselor';
  }

  get configCounselorPath() {
    return `${this.configRootPath}/${this.configCounselorUri}`;
  }

  get configPermissionUri() {
    return 'permission';
  }

  get configPermissionPath() {
    return `${this.configRootPath}/${this.configPermissionUri}`;
  }
}
